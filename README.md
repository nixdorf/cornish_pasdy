# CORNish PASDy -- COsmic-Ray Neutron flavored Processing and Analysis of Sensor Data in pYthon

This is a project developed by Martin Schr�n, Jannis Jakobi, Mandy Kasner, Rafael Rosolem,
in order to make Cosmic Ray Neutron Sensing Data Analysis accessible for everyone!
Please ask us about coauthorship if you use our code for a publication.

*Note:* This version of PASDy is specific for Cosmic-Ray Neutron Sensing (CRNS). A more general version of PASDy is to be developed in the far future.

## Contribution

- Did you find a bug or are you missing a feature? Please report your experience in the [project issues](https://git.ufz.de/CRNS/cornish_pasdy/issues).
- Have you resolved errors or added new features? Please push your contributions back to GitLab such that the whole community can benefit from your work. Contact [Martin](mailto:martin.schroen@ufz.de) to receive write permission.

## Usage

The CORNish PASDy package aims to allow various ways of usage depending on the level of detail and interaction required by individual users. It can be used in the following ways:

- **Mouse click** on the file `instantPASDy.py` processes the sensor data instantly. It requires a file `config.cfg` to be present locally. Use this option if you have only one type of sensor for which the exact same analysis needs to be performed over and over again.
- In the **console**, type
    
        python instantPASDy.py path/to/your/configfile.cfg
    
    Use this option if you don't like GUIs or you apply batch scripting. Add the parameter `--nonstop` to avoid the final input prompt at the end of the script.
- **Execute** `instantPASDy-GUI.py` where the config file can be selected and ammended directly through the interface.
- Work on your own scripts (e.g., using Jupyter notebooks) and **import** the required snippets from the PASDy package.  
    *(This option is currently under development)*
   

## Install

### Python 3.*

Get Python from here: <https://www.python.org/downloads/>

### Packages

Find out what packages you need to install or update:

    pip install pipreqs
    pipreqs ./
    
The list of required packages is saved in `requirements.txt`. Install all of them using:

    pip install -r requirements.txt

Examples for important packages used by PASDy are:  qgrid, utm, tzlocal, pympler, os, sys, inspect, math, numpy, matplotlib, datetime, rasterio, IPython.display, pandas, warnings, re, cartopy, netCDF4, requests, configparser, utm, copy, itertools.

If the installation of some modules fails, try out downloading and installing the corresponding wheel file directly from <https://www.lfd.uci.edu/~gohlke/pythonlibs/>.


### Jupyter Lab

Jupyter notebooks are a great way to play with python, here is a short instruction of installation:

    pip install jupyter
    pip install jupyterlab
    jupyter nbextension enable --py --sys-prefix widgetsnbextension
    jupyter labextension install @jupyterlab/git
    pip install --upgrade jupyterlab-git
    jupyter serverextension enable --py jupyterlab_git
      
Apply beautiful styles:

1. copy the file `docs/custom.css` into your home folder to `~/.jupyter/custom/`
2. copy the file `docs/pasdy.mplstyle` into your home folder to `~/.matplotlib/stylelib/`
3. Install the `Handlee` font family: <https://fonts.google.com/specimen/Handlee?selection.family=Handlee>

Run Jupyter Lab:

    jupyter lab

