import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import pandas
from dateutil import tz, parser

# Takes a data frame and plots all columns
def plot_all(D, nrow=1, ncol=4, skip=[]): 
    fig = plt.figure(figsize=(ncol*4, nrow*3))
    fig.subplots_adjust(hspace=0.4, wspace=0.3)
    i = 0
    k = 0
    for i in D.columns:
        if not i in skip:
            k += 1
            plt.subplot(nrow, ncol, k)
            ax = D[i].plot(title=i)
            ax.set_xlabel("")

# aggregate data frame
def minly( D, agg=1): return(D.resample('%iMin' % agg).mean())
def hourly(D, agg=1): return(D.resample('%iH' % agg).mean())
def  daily(D, agg=1):
    D = D.resample('%iD' % agg).mean()
    D.index.freq = 'D'
    return(D)           
def minlys( D, agg=1, min_count=0): return(D.resample('%iMin' % agg).sum(min_count=min_count))
def hourlys(D, agg=1, min_count=0): return(D.resample('%iH' % agg).sum(min_count=min_count))
def  dailys(D, agg=1, min_count=0): return(D.resample('%iD' % agg).sum(min_count=min_count))

# cut period to that of another data frame
def cut_period_to(D, a, b=None):
    if b is None:
        b = D.index.max()
    # Cut to other DataFrame
    if isinstance(a, pandas.DataFrame):
        return(D.loc[a.index.min():a.index.max()])
    # Strings to parse
    elif isinstance(a, str):
        return(D.loc[parser.parse(a):parser.parse(b)])
    # Datetimes
    else:
        return(D.loc[a:b])


# correction for air pressure
def correct_p(p, pref=None, beta=132):
    if pref is None:
        pref = p.mean()
    return(np.exp((p - pref) / beta))

def correct_h(h, href=0):
    return(1 + 0.0054* (h-href))

def correct_i(i): # expects already relative values
    return(1/i)

def rh2ah(rh, Tem):
    return(6.112*np.exp(17.67*Tem/(243.5+Tem))/(273.15+Tem) * 2.1674 * rh)

def sm2N(sm, N0, off=0.02, bd=1, a0=0.0808, a1=0.115, a2=0.372):
    return(N0*(0.0808/(sm/bd+0.115+off)+0.372))

def sm2N_Koehli(sm, h=9, off=0.02, bd=1, func='vers1', method=None, bio=0):
    #vers1: Sep25_responsef, Sep25_Ewindow, vers2: Jan23_uranos, Jan23_mcnpfull, Mar12_atmprof
    
    # total sm
    smt = sm + off
    smt *= 1.43/bd
    # nothing to do with bd
    p = []

    ################# PLEASE DOUBLE-CHECK THE FUNCTIONS AND VALUES ##################
    
    if func == 'vers1':
        if method == 'Sep25_responsef':          p = [4.179, 0.0230, 0.200, 1.436, 0.902, -0.00308, -0.0716, -0.0000163, 0.00164]
        elif method == 'Sep25_Ewindow':          p = [8.284, 0.0191, 0.143, 2.384, 0.760, -0.00344, -0.1310, -0.0000240, 0.00280]
        
        N = (p[1]+p[2]*smt)/(smt+p[1])*(p[0]+p[6]*h +p[8]* h**2+p[7]*h**3) + np.exp(-p[3]*smt)*(p[4]+p[5]*h)
    
    elif func == 'vers2':
        if method == 'Jan23_uranos':             p = [4.2580, 0.0212, 0.206, 1.776, 0.241, -0.00058, -0.02800, 0.0003200, -0.0000000180]
        elif method == 'Jan23_mcnpfull':         p = [7.0000, 0.0250, 0.233, 4.325, 0.156, -0.00066, -0.01200, 0.0004100, -0.0000000410]
        elif method == 'Mar12_atmprof':          p = [4.4775, 0.0230, 0.217, 1.540, 0.213, -0.00022, -0.03800, 0.0003100, -0.0000000003]
        
        elif method == 'Mar21_mcnp_drf':         p = [1.0940, 0.0280, 0.254, 3.537, 0.139, -0.00140, -0.00880, 0.0001150,  0.0000000000]
        elif method == 'Mar21_mcnp_ewin':        p = [1.2650, 0.0259, 0.135, 1.237, 0.063, -0.00021, -0.01170, 0.0001200,  0.0000000000]
        elif method == 'Mar21_uranos_drf':       p = [1.0240, 0.0226, 0.207, 1.625, 0.235, -0.00290, -0.00930, 0.0000740,  0.0000000000]
        elif method == 'Mar21_uranos_ewin':      p = [1.2230, 0.0185, 0.142, 2.568, 0.155, -0.00047, -0.01190, 0.0000920,  0.0000000000]
        
        elif method == 'Mar22_mcnp_drf_Jan':     p = [1.0820, 0.0250, 0.235, 4.360, 0.156, -0.00071, -0.00610, 0.0000500,  0.0000000000]
        elif method == 'Mar22_mcnp_ewin_gd':     p = [1.1630, 0.0244, 0.182, 4.358, 0.118, -0.00046, -0.00747, 0.0000580,  0.0000000000]
        elif method == 'Mar22_uranos_drf_gd':    p = [1.1180, 0.0221, 0.173, 2.300, 0.184, -0.00064, -0.01000, 0.0000810,  0.0000000000]
        elif method == 'Mar22_uranos_ewin_chi2': p = [1.0220, 0.0218, 0.199, 1.647, 0.243, -0.00029, -0.00960, 0.0000780,  0.0000000000]
        elif method == 'Mar22_uranos_drf_h200m': p = [1.0210, 0.0222, 0.203, 1.600, 0.244, -0.00061, -0.00930, 0.0000740,  0.0000000000]
        
        N = (p[1]+p[2]*smt)/(smt+p[1])*(p[0]+p[6]*h+p[7]*h**2+p[8]*h**3/smt)+np.exp(-p[3]*smt)*(p[4]+p[5]*(h + bio/5*1000))
    
    return(N/N.mean())

from hydroeval import evaluator, kge, rmse, nse
def Performance(data, title='Test', writeout=True):
    columns = data.columns
    D = data.copy().dropna().values
    k = evaluator(kge,  D[:,0], D[:,1]).flatten()
    r = evaluator(rmse, D[:,0], D[:,1])
    n = evaluator(nse,  D[:,0], D[:,1])
    if writeout:
        print('%35s: KGE %.3f, NSE %.3f, RMSE %.3f, Correlation %.3f, Variance %.3f, Bias %.3f' %
            (columns[0]+' vs. '+columns[1], k[0], n[0], r[0], k[1], k[2], k[3]))
    else:
        return([k[0], n[0], r[0], k[1], k[2], k[3]])
    
def plot_errorbar(x, y, xerr=None, yerr=None, fmt='s', ecolor='black', elinewidth=1, capsize=3, mfc='white', mec='black', ms=7, mew=1, alpha=1):
    return(plt.errorbar(x, y, xerr=xerr, yerr=yerr,
                        fmt=fmt, ecolor=ecolor, elinewidth=elinewidth, capsize=capsize,
                        mfc=mfc, mec=mec, ms=ms, mew=mew, alpha=alpha))

def D86(sm, bd=1, r=1):
    return(1/bd*( 8.321+0.14249*( 0.96655+np.exp(-r/100))*(26.42+sm) / (0.0567+sm)))

def Weight_d(d, D):
    return(np.exp(-2*d/D))

def read_files(pattern, index_col=1, skiprows=14):
    from glob import glob
    D = None
    for f in glob(pattern):
        Dx = pandas.read_csv(f, index_col=index_col, parse_dates=True, infer_datetime_format=True,
                             skipinitialspace=True, sep=',', skiprows=skiprows, encoding="cp850",
                       names=['RecordNum','t','#ClkTicks','PTB110_mb','P4_mb','P1_mb','Vbat','T1_C','RH1',
                              'N1Cts','N2Cts','N1ETsec','N2ETsec','N1T(C)','N1RH','N2T(C)','N2RH','T_CS215','RH_CS215',
                              'GpsUTC','LatDec','LongDec','Alt','Qual','NumSats','HDOP',
                              'Speed_kmh','COG','SpeedQuality','strDate'])
        if D is None:
            D = Dx.copy()
        else:
            D = pandas.concat([D, Dx])
    return(D)
    #D.head(10)
    
    
import re
re_crnsdata = re.compile(r'^\d+\,')
re_dataselect = re.compile(r'//DataSelect\s*=\s*(\w+)')
re_columns = re.compile(r'/+(RecordNum.+)')
re_columns2 = re.compile(r'/+(GpsUTC.+)')

def read_file(filename, archive=None, tz='UTC'):
    header_skip = 0
    datastr = ''
    with archive.open(filename,'r') if archive else open(filename, encoding="cp850") as file:
        for line in file:
            if isinstance(line, bytes):
                line = line.decode(errors='ignore')
            if re_crnsdata.search(line):
                datastr += line
    return(datastr+"\n")

import io
import os
import zipfile

def read(path, prefix='', suffix='', data_columns=[], progress='file'):

    archive = None
    files = []

    if path.endswith('.zip'):
        archive = zipfile.ZipFile(path, 'r')
        files = [x for x in archive.namelist() if x.endswith(suffix) and x.startswith(prefix)]

    elif os.path.isdir(path):
        pattern = path + prefix +'*'+ suffix
        files = glob(pattern)
        if len(files)==0:
            print("ERROR: Folder is empty: " + pattern)
            return(None)
    else:
        print("ERROR: Input is neither a file, folder, nor a zip archive.")
        return(None)
        
    datastr = ''
    i = 0
    last_progress = 0 # in %
    i_max = len(files)
    
    for filename in files:
        i += 1
        
        if progress == 'percent':
            this_progress = (i+0.01)/len(files)//0.1*10 # in %, offset 0.01 because x/x//0.1 is 9
            if this_progress > last_progress:
                print("%.0f%% " % this_progress, end='')
                last_progress = this_progress

        elif progress == 'file':
            print('%5d/%d: %s' % (i, i_max, filename))

        if datastr=='':
            #if data_columns == []:
            #    data_columns = read_header(filename, archive)
            datastr = read_file(filename, archive)
        else:
            datastr += read_file(filename, archive) #data.append() #, verify_integrity=True)

    if not archive is None:
        archive.close()
    
    #DEBUG output raw data file
    #with open('temp.txt','a') as f:
    #    f.write(datastr)
    
    data = make_pandas(datastr, data_columns)
    if data is None:
        print('Error: Cannot interprete the data. Check the column header, is this correct? '+ ','.join(data_columns))
        return()
    else:
        return(data)

def make_pandas(datastr, file=False, columns=None, header_skip=0, index=1):
    if file:
        f = datastr
    else:
        f = io.StringIO(datastr)
        
    try:
        data = pandas.read_csv(f, index_col=index, parse_dates=True, infer_datetime_format=True, skipinitialspace=True,
                               sep=',', skiprows=12, encoding="cp850", error_bad_lines=False, warn_bad_lines=False, names=columns, dtype=object)
    except Exception as e:
        print("ERROR interpreting data format: "+str(e))
        return(None)
    
    data = data.apply(pandas.to_numeric, errors='coerce')
    return(data)    

def read_cornish(file):
    data = pandas.read_csv(file, index_col=0, parse_dates=True, infer_datetime_format=True, skipinitialspace=True,
                           sep=',', encoding="cp850", error_bad_lines=False, warn_bad_lines=False) #, dtype=object)
    print("%i lines." % len(data))
    return(data)

def read_geojson(file, columns=['label','short','type','lat','lon']):
    import geojson
    with open(file) as f:
        gj = geojson.load(f)
        
    # Run through geojson and fill a Sensor DataFrame
    D = pandas.DataFrame([], columns=columns)
    for i in range(len(gj['features'])):
        Dx = pandas.DataFrame([[
            gj['features'][i]["properties"]['Id'],
            gj['features'][i]["properties"]['short'],
            gj['features'][i]["properties"]['Inst'],
            gj['features'][i]["geometry"]["coordinates"][1],
            gj['features'][i]["geometry"]["coordinates"][0]]],
            columns=columns)
        D = D.append(Dx)
    D = D.sort_values('label')
    D['color'] = 'black'
    D = D.set_index(np.arange(1,len(D)+1))
    return(D)

import cartopy
import cartopy.feature as cpf
import cartopy.crs as ccrs
import matplotlib as mpl
from matplotlib.pyplot import figure, show
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

def Shapemap(lons=None, lats=None, var=None, extent=[5.5,15.5,47.2,55.2], size=(6,4), method=None,
             contour_levels=None, title='', points=None, grid=False, cmap_name='Spectral', colorbar=True,
             save=None, save_dpi=300, point_size=1, ticks_sep=0.05, clim=None,
             resolution='50m', resolution_fine='10m', counties=True,
             ocean=True, land=False, lakes=True, rivers=True, borders=True, coast=True):
    
    fig, ax = plt.subplots(figsize = size)
    ax = plt.axes(projection=cartopy.crs.PlateCarree())
    if title: ax.set_title(title)

    if clim is None: clim = (None, None)
    colormap = mpl.cm.get_cmap(cmap_name)
    #colormap.set_over("red")
    #colormap.set_under("blue")
    if method=='contour':
        im = ax.contourf(lons, lats, var, levels=contour_levels,
                          transform=ccrs.PlateCarree(), cmap=colormap,
                          extend="both",    # do not go beyond zmin/zmax
                          antialiased=False)
        im.cmap.set_under('white')
    elif method=='raster':
        im = ax.pcolormesh(lons, lats, var, transform=ccrs.PlateCarree(), cmap=colormap, rasterized=True)
        im.cmap.set_under('white')
    elif method=='points':
        im = ax.scatter(lons, lats, c=var, cmap=colormap, vmin=clim[0], vmax=clim[1], s=point_size)
        im.cmap.set_under('white')
        #ax.stock_img()
    elif method=='lines':
        im = ax.plot(lons, lats, color='red')
        #ax.stock_img()
    

    # Features
    if ocean:   ax.add_feature(cpf.OCEAN.with_scale(resolution)) # ocean background and coast shape 
    if land:    ax.add_feature(cpf.LAND.with_scale(resolution))  # land background and shape
    if lakes:   ax.add_feature(cpf.LAKES.with_scale(resolution_fine))
    if rivers:  ax.add_feature(cpf.RIVERS.with_scale(resolution_fine), lw=0.5)
    if counties:
        counties = cpf.NaturalEarthFeature(category='cultural', name='admin_1_states_provinces_lines',
            scale=resolution_fine, facecolor='none')
        ax.add_feature(counties, edgecolor='black', lw=0.5, alpha=0.5)
    if borders: ax.add_feature(cpf.BORDERS.with_scale(resolution), lw=0.5)
    if coast:   ax.add_feature(cpf.COASTLINE.with_scale(resolution), lw=0.5)

    
    # Points
    if not points is None:
        for i, p in points.iterrows():
            ax.scatter(p['lon'], p['lat'], lw=1, s=10, color=p['color'])
            ax.text(p['lon']+0.01*(extent[1]-extent[0]), p['lat'], str(p['label']), color=p['color'], verticalalignment='center')

    # Grid
    gl = ax.gridlines(draw_labels=True, color="black", alpha=0.2, lw=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlines = grid
    gl.ylines = grid
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    #gl.xlocator = mticker.FixedLocator(np.arange(int(extent[0]),int(extent[1])+1))
    #gl.ylocator = mticker.FixedLocator(np.arange(int(extent[2]),int(extent[3])+1))
    gl.xlocator = mticker.FixedLocator(np.arange(np.round(extent[0],2),np.round(extent[1],2)+1, ticks_sep))
    gl.ylocator = mticker.FixedLocator(np.arange(np.round(extent[2],2),np.round(extent[3],2)+1, ticks_sep))
    gl.xlabel_style = {'size': '7'}
    gl.ylabel_style = {'size': '7'}

    if colorbar and not method is None:
        cb = plt.colorbar(im, extend="both")
        cb.set_label(title, rotation=270, labelpad=20)

    ax.set_extent(extent)
    
    if not save is None:
        plt.savefig(save, bbox_inches='tight', frameon=False, dpi=save_dpi)
        plt.close(fig)
    
    return(ax)

import rasterio
#pandas.options.mode.chained_assignment = None  # Skip annoying warnings

def get_from_raster(data, var, file):

    if var == 'bd':
        def function(x): return(float(x)/1000)
    elif var == 'clay':
        def function(x): return(float(x)/100)
    elif var == 'org':
        def function(x): return(float(x)/1000)
    elif var == 'luse':
        def function(x): return(int(x))
    else:
        def function(x): return(float(x))

    with rasterio.open(file) as src:
        data[var] = [function(x) for x in src.sample(zip(data.lon, data.lat))]

    if var == 'luse':
        # TODO: this should be a variable with its properties and methods
        luse_cat = ['urban', 'agriculture', 'wetland', 'forest', 'water']
        luse_col = ['red', 'orange', 'lightblue', 'green', 'blue']    
        luse_corineid = [1,2,4,3,5]

        data['luse_str'] = np.nan
        for i in np.arange(len(luse_cat)):
            data.loc[data['luse'] // 100 == luse_corineid[i], 'luse_str'] = luse_cat[i]
    
    return(data)

# Make boxplots of something over luse data
# Requires .get_from_raster('luse', file) to be called beforehand
def luse_plot(data, var, label=None, format_str="%.2f",
              luse_cat=['urban', 'agriculture', 'wetland', 'forest', 'water'],
             luse_col=['red', 'orange', 'lightblue', 'green', 'blue']):
    data = [data.loc[data.luse_str==l, var].dropna() for l in luse_cat]
    with Fig(size=(5,4), grid=False, legend=False):
        B = Fig.ax.boxplot(data, patch_artist=True, medianprops=dict(linestyle='--', color='black', linewidth=1))
        for b, color in zip(B['boxes'], luse_col):
            b.set(color=color, facecolor=color)
        for i, m in enumerate([x.median() for x in data]):
            if np.isfinite(m):
                text = Fig.ax.annotate(format_str % m, (1.4+i, m), color="black", ha='center')
                text.set_rotation(270)
        Fig.ax.set_xticklabels(luse_cat)
        if label is None: label = var
        Fig.ax.set_ylabel(label) #("%s in %s" % (getattr(X, var).name, getattr(X, var)._units))
        
import os
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import dates
import cartopy              

### Usage:
# with Fig((2,2)):
#     with Fig(): X.T1.plot()
#     with Fig(): X.N1_T.plot()
#     with Fig(): X.p1.plot()
#     with Fig(): X.p4.plot()
# Fig.save('plots/test.pdf')
###


#class Fig:
#    fig = None # fig object given by pdfpages
#    ax = None  # current single axis
#    time_format = '%Y'
#    layout = (1,1)
#    axi = 1
#    submode = False

#    def __init__(self, fig=None, title='', layout=(1,1), xlabel='', ylabel='', size=(11.69,8.27), ylim=None, time_series=True):

        # Single PDF page when fig is provided
#        if not fig is None:
#            Fig.fig = fig
#            plt.figure(figsize=size)
#            Fig.layout = layout
#            Fig.axi = 0
        
#        if layout != (1,1):
            # For complex layout, do not do anything, just wait for next "with Fig()"
#            Fig.submode = True
#        else:
#            Fig.axi += 1
#            Fig.ax = plt.subplot("%i%i%i" % (Fig.layout[0], Fig.layout[1], Fig.axi))
            
#            plt.title(title)
#            Fig.ax.set(xlabel=xlabel, ylabel=ylabel)
#            if ylim: Fig.ax.set_ylim(ylim)
#            for a in ("top", "right"):
#                Fig.ax.spines[a].set_visible(False)
#                Fig.ax.get_xaxis().tick_bottom()
#                Fig.ax.get_yaxis().tick_left()
#            Fig.ax.grid(b=True, alpha=0.2)
#            time_format = re.sub(r'(\w)', r'%\1', Fig.time_format)
#            if time_series:
#                Fig.ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter(time_format.replace('\\','')))
#                plt.tick_params(labelsize=9)
    
    # Entering `with` statement
#    def __enter__(self):
#        return(Fig.ax) # makes possibe: with Fig() as ax: ax.change
    
    # Exiting `with` statement    
#    def __exit__(self, type, value, traceback):
        # deactivate submode if axis counter exceeds layout shape
#        if Fig.submode:
#            if Fig.axi == Fig.layout[0]*Fig.layout[1]:
#                Fig.submode = False
#        else:
            # save and close PDF page
#            Fig.fig.savefig(bbox_inches='tight')
#            plt.close()
            
            
class Fig:
    fig = None
    axes = []  # 2D array of axes
    ax = None  # current single axis
    axi = None # flat counter for Fig.axes[axi] === ax
    legend = 1
    time_format = '%b %d'
    leftbot_axes = True
    #grid = True
    #layout = (1,1)
    #sharex = True
    #sharey = False
    submode = False
    axcount = 0
    #style = '.-'
    args = {'style':'.-', 'legend': 1, 'grid':True, 'layout':(1,1), 'sharex':True, 'sharey':False, 'proj':False}


    # This is a fake instance, this class always acts with static methods and attributes
    def __init__(self, title='', layout=(1,1), **kwargs):
        # Magic recognition of Fig((1,2),'title')
        if isinstance(title, tuple):
            layout, title = title, layout
            if not isinstance(title, str): title = ''
    
        # If sub figures are expected
        #print(Fig.submode)
        if Fig.submode:
            Fig.sub(title, **kwargs)
        else:
            Fig.new(title, layout=layout, **kwargs)
    
    # Entering `with` statement
    def __enter__(self):
        return(Fig.ax) # makes possibe: with Fig() as ax: ax.change
    
    # Exiting `with` statement    
    def __exit__(self, type, value, traceback):
    
        # hide redundant y and x bars
        if Fig.args['sharex'] and Fig.axes.shape[0] > 1:
            plt.setp([a.get_xticklabels() for a in Fig.axes[0, :]], visible=False)
        if Fig.args['sharey']:
            plt.setp([a.get_yticklabels() for a in Fig.axes[:, 1]], visible=False)
            
        # make suptitle arrange nicely
        if Fig.args['tight']:
            Fig.fig.tight_layout()
        if Fig.args['layout'] != (1,1):
            Fig.fig.subplots_adjust(top=0.88)
        
        if Fig.axi >= Fig.axcount-1: # Fig.fig.axes can be actually larger if colorbars were added
            Fig.submode = False
        
        # Make legend without border 
        if Fig.args['legend']:
            try:
                Fig.ax.legend(loc=int(Fig.args['legend'])).get_frame().set_linewidth(0.0)
            except:
                pass
        
        #Fig.fig.show()
        
    
    # Redirect Fig.x to Fig.fig.x
    def __getattr__(self, name):
        try:
            return(getattr(Fig.fig, name))
        except:
            raise AttributeError('Sorry, neither Fig nor Fig.fig have such attribute.')
        
    
    @staticmethod
    def new(title='', time_format=None, layout=None, size=(12,4), leftbot_axes=None,
            legend=None, grid=None, sharex=None, sharey=None, ylim=None, xlim=None,
            proj=None, tight=True, time_series=True, **kwargs):
        
        if layout is None: layout = Fig.args['layout']; Fig.args['layout'] = layout
        if sharex is None: sharex = Fig.args['sharex']; Fig.args['sharex'] = sharex
        if sharey is None: sharey = Fig.args['sharey']; Fig.args['sharey'] = sharey
        Fig.args['legend'] = legend
        Fig.args['grid'] = grid
        Fig.args['tight'] = tight
        Fig.args['time_series'] = time_series
        
        subplot_dict = {}
        if proj=='cartopy':
            subplot_dict['projection'] = cartopy.crs.PlateCarree()
            Fig.args['proj'] = True
            Fig.args['tight'] = False
        
        Fig.fig, Fig.axes = plt.subplots(layout[0],layout[1], figsize=size, squeeze=False, subplot_kw=subplot_dict)
        Fig.axcount = len(Fig.fig.axes)
        Fig.axi = -1
        
        if layout == (1,1):
            Fig.sub(title, time_format=time_format, leftbot_axes=leftbot_axes, legend=legend, grid=grid, xlim=xlim, ylim=ylim, tight=tight, time_series=time_series, **kwargs)
        else:
            Fig.submode = True
            plt.suptitle(title, fontweight='normal')
            
        return(Fig)
        
    
    @staticmethod
    def sub(title='', time_format=None, leftbot_axes=None,
            legend=None, grid=None, xlim=None, ylim=None,
            proj=None, tight=None, time_series=None, **kwargs):
        # Defaults    
        if time_format  is None: time_format = Fig.time_format 
        if leftbot_axes is None: leftbot_axes = Fig.leftbot_axes
        if legend       is None: legend = Fig.args['legend']
        if grid         is None: grid = Fig.args['grid']
        if tight         is None: tight = Fig.args['tight']
        if time_series  is None: time_series = Fig.args['time_series']
        
        
        # New axis
        #Fig.fig, Fig.axes = plt.subplots(layout[0],layout[1], squeeze=False)
        Fig.axi += 1
        print('Axes', str(len(Fig.fig.axes)), 'axi', Fig.axi)
        Fig.ax = Fig.fig.axes[Fig.axi]
        plt.sca(Fig.ax)
        
        if not ylim is None: Fig.ax.set_ylim(ylim)
        if not xlim is None: Fig.ax.set_xlim(xlim)
        
        xlabel = 'x'
        xlabel = 'y'
        if 'xlabel' in kwargs: xlabel = kwargs.get('xlabel')
        if 'ylabel' in kwargs: ylabel = kwargs.get('ylabel')
        Fig.ax.set(xlabel=xlabel, ylabel=ylabel)
        
        if time_series:
            Fig.ax.xaxis_date() # treat x axis as date (fixes some bugs)
        Fig.ax.grid(b=grid)
        Fig.ax.set_title(title, fontweight='bold')
        
        if leftbot_axes:
            for a in ("top", "right"):
                #for i in len(range(Fig.ax.size)):
                Fig.ax.spines[a].set_visible(False)
                Fig.ax.get_xaxis().tick_bottom()
                Fig.ax.get_yaxis().tick_left()
        
        # Time Format
        if time_series:
            time_format = interpret_time_format(time_format)
            Fig.ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter(time_format))
        
        # Legend
        #if not legend:
        #Fig.args['legend'] = False
        #if legend >= 0:
        #    Fig.args['legend'] = int(legend)
        
        return(Fig.ax)
        
    
    @staticmethod
    def save(file='plots/test.pdf', dpi=300, tight=False, overwrite=True, **kwargs):
        # make layout tight to arrange margins 
        if tight: Fig.fig.tight_layout()
        
        # Create folder
        folder = os.path.dirname(os.path.abspath(file))
        if not os.path.exists(folder):
            os.makedirs(folder)
            print('Note: Folders were created:', folder)
            
        # Number files to avoid overwriting
        if not overwrite and os.path.exists(file):
            i = 0
            file_path, file_ext = os.path.splitext(file)
            while os.path.exists('{}-{:d}{}'.format(file_path, i, file_ext)):
                i += 1
            file = '{}-{:d}{}'.format(file_path, i, file_ext)
        
        # Savefig
        Fig.fig.savefig(file, dpi=dpi, **kwargs)
        
        # Print size
        size = "%.1f KB" % (os.path.getsize(file)/1024)
        print('Saved "'+file+'" ('+size+')')
        
        
def interpret_time_format(s):
    s = s.replace('monthname', '%b').replace('month', '%M')
    s = s.replace('day', '%d').replace('hour', '%H').replace('year', '%Y')
    s = s.replace('minute', '%M').replace('min', '%M')
    return(s)

def map(data, var="N", center=None, zoom=17, tiles='openstreetmap', colormap='Spectral', collim=None,
        size=4.5, features=['points','shadow','border','gleam'], luse=False, shadow_factor=1.75, border_factor=1.25):

    luse_cat=['urban', 'agriculture', 'wetland', 'forest', 'water']
    luse_col=['red', 'orange', 'lightblue', 'green', 'blue']
    
    import folium
    #if data is None:
    data = data.dropna(subset=[var])
    #data = data.dropna()
    if center is None:
        center = [data.lat.mean(), data.lon.mean()]
        #[51.352042, 12.431250]
    if collim is None:
        xmin = np.min(data.loc[:,var])
        xmax = np.max(data.loc[:,var])
    else:
        xmin = collim[0]
        xmax = collim[1]

    #data['color'] = ''
    data.loc[:,'color'] = ''
    if luse:
        for i in range(len(luse_cat)):
            data.loc[data.luse_str==luse_cat[i], "color"] = luse_col[i]
        #data['color'] = self.luse_col[self.luse_cat.index(data['luse_str'])] #self.luse_col[data['luse']//100]
    else:
        cmap = matplotlib.cm.get_cmap(colormap)
        for i, row in data.iterrows():
            tmp = (data.at[i,var]-xmin)/(xmax-xmin)
            data.at[i,'color'] = matplotlib.colors.rgb2hex(cmap(tmp)[:3])
    #print(data)
    M = folium.Map(location=center, zoom_start=zoom, tiles=tiles)

    if any(f in ['border','shadow'] for f in features):
        for i, row in data.iterrows():
            if 'shadow' in features:
                folium.Circle(location=[row["lat"], row["lon"]], radius=size*shadow_factor, color=None, fill_color="black", fill_opacity=0.2).add_to(M)
            if 'border' in features:
                folium.Circle(location=[row["lat"], row["lon"]], radius=size*border_factor, color=None, fill_color="black", fill_opacity=1).add_to(M)

    if any(f in ['points'] for f in features):        
        for i, row in data.iterrows():
            folium.Circle(location=[row["lat"], row["lon"]], radius=size, color=None, fill_color=row["color"], fill_opacity=1).add_to(M)
            if 'gleam' in features:
                folium.Circle(location=[row["lat"]+0.00001*size/3, row["lon"]+0.00001*size/3], radius=size/2, color=None, fill_color="white", fill_opacity=0.4).add_to(M)

    if 'lines' in features:
        folium.PolyLine(list(data.loc[:,["lat","lon"]].itertuples(index=False, name=None)), color="black", weight=1, opacity=1).add_to(M)

    return(M)


def report_size(file, verb='Saved', unit='MB'):
    import os
    if unit=='KB':
        factor = 1/1024
    elif unit=='MB':
        factor = 1/1024/1024
    else:
        factor = 1
        unit = 'unknown'
    size = os.stat(file).st_size * factor
    print("%s %s (%.1f %s)." % (verb, file, size, unit))

### OSMNX Graphs ####################################

import osmnx as ox
ox.utils.config(data_folder='data')
import os

class Graph:
    def __init__(self, network='roads', bbox=None, lats=None, lons=None, file=None, report_save='Saved'):
        
        self.bbox = self.make_bbox(bbox, lats, lons)
        self.network = network
        if file is None:
            file = self.network + '.graphml'
        self.file = file
        self.report_save = report_save
        
        if self.network == 'roads':
            self.lw = 1
            self.ls = '-'
            self.color = 'black'
            self.alpha = 0.1
            self.network_type='drive'
            self.infrastructure='way["highway"]'
            self.custom_filter='["highway"~"residential|track|service|motorway|trunk|primary|secondary|tertiary|' \
                +'unclassified|road|motorway_link|trunk_link|primary_link|secondary_link|tertiary_link"]'
                
        elif self.network == 'rails':
            self.lw = 1
            self.ls = '--'
            self.color = 'black'
            self.alpha = 0.5
            self.network_type = 'none'
            self.infrastructure = 'way["railway"]'
            self.custom_filter = None
                
        elif self.network == 'rivers':
            self.lw = 1
            self.ls = '-'
            self.color = '#97B6E1'
            self.alpha = 0.1
            self.network_type = 'none'
            self.infrastructure = 'way["waterway"]'
            self.custom_filter = None
                
    def make_bbox(self, bbox=None, lats=None, lons=None):
        
        if bbox is None:
            if lats is None or lons is None:
                default_bbox = [11.030000, 11.06633, 51.640000, 51.670000]
                print('No lats/lons given, assuming bbox', default_bbox)
                return(default_bbox)
            else:
                lats = list(lats)
                lons = list(lons)
                return([min(lons), max(lons), min(lats), max(lats)])
        else:
            return(bbox)

    def download(self, bbox=None, network_type=None, infrastructure=None, custom_filter=None,
                 timeout=180, memory=None, max_query_area_size=2500000000):

        #ox.utils.config(all_oneway=True)

        if bbox is None: bbox = self.bbox 
        if network_type is None: network_type = self.network_type 
        if infrastructure is None: infrastructure = self.infrastructure 
        if custom_filter is None: custom_filter = self.custom_filter 
            
        print('Downloading OSM network graph...', end='')
        self.G = ox.graph_from_bbox(bbox[3], bbox[2], bbox[1], bbox[0],
                                       simplify=True, truncate_by_edge=True, retain_all=True,
                                       network_type  =network_type,
                                       custom_filter =custom_filter)
        self.data = self.graph2gdf()

        if len(self.data)<=0:
            print(' no roads found!')
        else:
            print(' OK.')
        return(self)
    def export_csv(self, prefix=''):
        file_nodes = ox.settings.data_folder + '/' + prefix + 'nodes.csv'
        file_edges = ox.settings.data_folder + '/' + prefix + 'edges.csv'
        self.graph2gdf(nodes=True, edges=False)[['y','x','osmid']].to_csv(file_nodes)
        report_size(file_nodes, self.report_save)
        self.graph2gdf(nodes=False, edges=True)[['u','v','osmid','highway','oneway','length']].to_csv(file_edges)
        report_size(file_edges, self.report_save)

    def save(self, file=None):
        if file is None:
            file = self.file
        else:
            self.file = file
            
        if self.G:
            ox.save_load.save_graphml(self.G, file)
            report_size(ox.settings.data_folder+'/'+file, self.report_save)
        
        return(self)
            
    def load(self, file=None):
        if file is None:
            file = self.file
        else:
            self.file = file
            
        if os.path.exists(ox.settings.data_folder+'/'+file):
            report_size(ox.settings.data_folder+'/'+file, 'Loading')
            self.G = ox.save_load.load_graphml(file)
            self.data = self.graph2gdf()
            
        return(self)
            

    def graph2gdf(self, graph=None, nodes=False, edges=True):
        if graph is None:
            graph = self.G
        gdf = ox.graph_to_gdfs(graph, nodes=nodes, edges=edges, fill_edge_geometry=True)
        return(gdf)

    def get(self, file=None, network_type=None, infrastructure=None, custom_filter=None, save=False):
        if file is None:
            file = self.file
        else:
            self.file = file
            
        if os.path.exists(ox.settings.data_folder+'/'+file):
            #and network_type==self.network_type and infrastructure==self.infrastructure and custom_filter==self.custom_filter:
            #print('> Loading network file %s' % file)
            self.load(file)
        else:
            self.download(network_type=network_type, infrastructure=infrastructure, custom_filter=custom_filter)
            if save:
                self.save(file)
        
        return(self)
            
    # Plot
    
    def plot(self, ax=None, lw=None, ls=None, color=None, alpha=None):
        
        if lw is None:    lw =    self.lw
        if ls is None:    ls =    self.ls
        if color is None: color = self.color
        if alpha is None: alpha = self.alpha
         
        if ax is None:
            ox.plot_graph(self.G, edge_linewidth=lw, edge_color=color, edge_alpha=alpha)
        else:
            self.data.plot(ax=ax, lw=lw, ls=ls, color=color, alpha=alpha)
    
    # Analysis
    
    def nearest(self, point=(0,0)):
        if 'geometry' in self.data:
            self.data['distance'] = [ox.Point(tuple(reversed(point))).distance(line) for line in self.data['geometry']]
            return(min(list(set(self.data['distance'].values.tolist()))))
        else:
            self.data['distance'] = np.nan
            return(np.nan)
            
    def on_road_type(self, point=(0,0), limit=0.0001):
        distance = self.nearest(point)
        type_of_nearest = ''
        if distance < limit:
            type_of_nearest = self.data.loc[self.data['distance'] == distance, 'highway'].values[0]
        else:
            type_of_nearest = ''
        # flatten list according to https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-list-of-lists
        #print([item for sublist in type_of_nearest for item in sublist])
        return(type_of_nearest)


def latlon2utm(lats, lons):
    from pyproj import Transformer
    if isinstance(lats, pandas.DataFrame) or isinstance(lats, pandas.Series):
        lats = lats.values
        lons = lons.values
    utmy, utmx = Transformer.from_crs("epsg:4326", "epsg:31468").transform(lats, lons)
    return(utmy, utmx)

def utm2latlon(utmx, utmy):
    from pyproj import Transformer
    if isinstance(utmx, pandas.DataFrame) or isinstance(utmx, pandas.Series):
        utmx = utmx.values
        utmy = utmy.values
    lats, lons = Transformer.from_crs("epsg:31468", "epsg:4326").transform(utmy, utmx)
    return(lats, lons)
    
def Calibrate_N0_Desilets(N, sm, bd=1, lw=0, owe=0, a0=0.0808, a1=0.372, a2=0.115):
    return(N/(a0 / (sm/bd + a2 + lw + owe) + a1))

def N2SM_Desilets(N, N0, bd=1, lw=0, owe=0, a0=0.0808, a1=0.372, a2=0.115):
    return((a0/(N/int(N0)-a1)-a2 -lw - owe) * bd)

def abl1(N, N0=1000, a0=0.0808, a1=0.372, a2=0.115): return(   -a0*N0/(a1*N0-N)**2 )
def abl2(N, N0=1000, a0=0.0808, a1=0.372, a2=0.115): return( 2*-a0*N0/(a1*N0-N)**3 )
def abl3(N, N0=1000, a0=0.0808, a1=0.372, a2=0.115): return( 6*-a0*N0/(a1*N0-N)**4 )

def theta(N, N0=1000, a0=0.0808, a1=0.372, a2=0.115):
    return(a0/(N/N0-a1)-a0)
    
def dtheta_low(N, N_err=None, N0=1000, ordnung=3, a0=0.0808, a1=0.372, a2=0.115):
    if N_err is None: N_err = np.sqrt(N)
    r = 0
    if ordnung >= 1: r += N_err    *abl1(N, N0, a0, a1, a2)
    if ordnung >= 2: r += N_err**2 *abl2(N, N0, a0, a1, a2) /2
    if ordnung >= 3: r += N_err**3 *abl3(N, N0, a0, a1, a2) /6
    return( r )

def dtheta_upp(N, N_err=None, N0=1000, ordnung=3, a0=0.0808, a1=0.372, a2=0.115):
    if N_err is None: N_err = np.sqrt(N)
    r = 0
    if ordnung >= 1: r += -N_err    *abl1(N, N0, a0, a1, a2)
    if ordnung >= 2: r +=  N_err**2 *abl2(N, N0, a0, a1, a2) /2
    if ordnung >= 3: r += -N_err**3 *abl3(N, N0, a0, a1, a2) /6
    return( r )

def dtheta_std(N, N_err=None, N0=1000, ordnung=3, a0=0.0808, a1=0.372, a2=0.115):
    if N_err is None: N_err = np.sqrt(N)
    r = 0
    if ordnung >= 1: r += N_err**2 *abl1(N, N0, a0, a1, a2)**2
    if ordnung >= 2: r += N_err**4 *abl2(N, N0, a0, a1, a2)**2 /2
    if ordnung >= 3: r += N_err**6 *abl3(N, N0, a0, a1, a2)**2 /36*15 + N_err**4 *abl1(N, N0, a0, a1, a2) *abl3(N, N0, a0, a1, a2)
    return( np.sqrt(r) )

def dtheta_stdx(N, N_err=None, N0=1000, a0=0.0808, a1=0.372, a2=0.115):
    if N_err is None: N_err = np.sqrt(N)
    return( (a0 *N0 *N_err *np.sqrt(8 *N_err**2 *(N - a1 *N0)**2 + (N - a1 *N0)**4 + 15 *N_err**4))/(N - a1 *N0)**4 )

# Error propagation for factors
def errpropag_factor(data, A, B, err='_err', err2=None):
    if err2 is None: err2 = err
    return(data[A] * data[B+err2] + data[A+err] * data[B])
    # dz/z=dx/x+dy/y with z=xy => dz = dx*y+dy*x


# Moving average 
def moving_avg(D, column, window=1, err=True, std=True, center=True):
    window = int(window)
    suffix = '_mov' #+str(window)
    rollobj = D[column].rolling(window, center=center)
    D[column+suffix]                = rollobj.mean()
    if std: D[column+suffix+'_std'] = rollobj.std()
    if err: D[column+suffix+'_err'] = D[column+'_err'] * 1/np.sqrt(window)
    return(D)


def xyplot(x, y, z, resolution = 20, vrange = (0,0.5), contour_levels = np.arange(0.05,0.525,0.025), padding=0.05, colorbar=True,
            varlabel="vol. soil moisture (in m$^3$/m$^3$)", maptitle='Map', size=10, xlabel='Easting (in m)', ylabel='Northing (in m)'):
    x = x.values - x.min()
    y = y.values - y.min()
    z = z.values
    xrange = x.max()-x.min()
    yrange = y.max()-y.min()
    xpad = padding*xrange
    ypad = padding*yrange

    mysize = (size,size/xrange*yrange)
    if mysize[1] > size*2:
        mysize = (size*xrange/yrange,size)
    
    with Fig(title=maptitle, xlabel=xlabel, ylabel=ylabel, size=mysize, time_series=False) as ax:
        q = ax.scatter( x, y, c=z, cmap='Spectral', vmin=vrange[0], vmax=vrange[1])
        ax.set_aspect(1)
        ax.set_xlim(x.min()-xpad,x.max()+xpad)
        ax.set_ylim(y.min()-ypad,y.max()+ypad)
        if colorbar:
            cb = plt.colorbar(q, extend="both", ax=ax, shrink=0.6, pad=0.03, aspect=30)
            cb.set_label(varlabel, rotation=270, labelpad=20)
        plt.grid(color='black', alpha=0.1)
       
        
# Cosmetics
# Curved Text
from functions import *
from matplotlib import patches
from matplotlib import text as mtext
import math

class CurvedText(mtext.Text):
    """
    A text object that follows an arbitrary curve.
    From: https://stackoverflow.com/questions/19353576/curved-text-rendering-in-matplotlib
    """
    def __init__(self, x, y, text, axes, **kwargs):
        super(CurvedText, self).__init__(x[0],y[0],' ', **kwargs)
        axes.add_artist(self)

        # saving the curve:
        self.__x = x
        self.__y = y
        self.__zorder = self.get_zorder()

        # creating the text objects
        self.__Characters = []
        for c in text:
            if c == ' ':
                # make this an invisible 'a':
                t = mtext.Text(0,0,'a')
                t.set_alpha(0.0)
            else:
                t = mtext.Text(0,0,c, **kwargs)

            # resetting unnecessary arguments
            t.set_ha('center')
            t.set_rotation(0)
            t.set_zorder(self.__zorder +1)
            self.__Characters.append((c,t))
            axes.add_artist(t)


    ##overloading some member functions, to assure correct functionality
    ##on update
    def set_zorder(self, zorder):
        super(CurvedText, self).set_zorder(zorder)
        self.__zorder = self.get_zorder()
        for c,t in self.__Characters:
            t.set_zorder(self.__zorder+1)

    def draw(self, renderer, *args, **kwargs):
        """
        Overload of the Text.draw() function. Do not do
        do any drawing, but update the positions and rotation
        angles of self.__Characters.
        """
        self.update_positions(renderer)

    def update_positions(self,renderer):
        """
        Update positions and rotations of the individual text elements.
        """

        #preparations

        ##determining the aspect ratio:
        ##from https://stackoverflow.com/a/42014041/2454357

        ##data limits
        xlim = self.axes.get_xlim()
        ylim = self.axes.get_ylim()
        ## Axis size on figure
        figW, figH = self.axes.get_figure().get_size_inches()
        ## Ratio of display units
        _, _, w, h = self.axes.get_position().bounds
        ##final aspect ratio
        aspect = ((figW * w)/(figH * h))*(ylim[1]-ylim[0])/(xlim[1]-xlim[0])

        #points of the curve in figure coordinates:
        x_fig,y_fig = (
            np.array(l) for l in zip(*self.axes.transData.transform([
            (i,j) for i,j in zip(self.__x,self.__y)
            ]))
        )

        #point distances in figure coordinates
        x_fig_dist = (x_fig[1:]-x_fig[:-1])
        y_fig_dist = (y_fig[1:]-y_fig[:-1])
        r_fig_dist = np.sqrt(x_fig_dist**2+y_fig_dist**2)

        #arc length in figure coordinates
        l_fig = np.insert(np.cumsum(r_fig_dist),0,0)

        #angles in figure coordinates
        rads = np.arctan2((y_fig[1:] - y_fig[:-1]),(x_fig[1:] - x_fig[:-1]))
        degs = np.rad2deg(rads)


        rel_pos = 10
        for c,t in self.__Characters:
            #finding the width of c:
            t.set_rotation(0)
            t.set_va('center')
            bbox1  = t.get_window_extent(renderer=renderer)
            w = bbox1.width
            h = bbox1.height

            #ignore all letters that don't fit:
            if rel_pos+w/2 > l_fig[-1]:
                t.set_alpha(0.0)
                rel_pos += w
                continue

            elif c != ' ':
                t.set_alpha(1.0)

            #finding the two data points between which the horizontal
            #center point of the character will be situated
            #left and right indices:
            il = np.where(rel_pos+w/2 >= l_fig)[0][-1]
            ir = np.where(rel_pos+w/2 <= l_fig)[0][0]

            #if we exactly hit a data point:
            if ir == il:
                ir += 1

            #how much of the letter width was needed to find il:
            used = l_fig[il]-rel_pos
            rel_pos = l_fig[il]

            #relative distance between il and ir where the center
            #of the character will be
            fraction = (w/2-used)/r_fig_dist[il]

            ##setting the character position in data coordinates:
            ##interpolate between the two points:
            x = self.__x[il]+fraction*(self.__x[ir]-self.__x[il])
            y = self.__y[il]+fraction*(self.__y[ir]-self.__y[il])

            #getting the offset when setting correct vertical alignment
            #in data coordinates
            t.set_va(self.get_va())
            bbox2  = t.get_window_extent(renderer=renderer)

            bbox1d = self.axes.transData.inverted().transform(bbox1)
            bbox2d = self.axes.transData.inverted().transform(bbox2)
            dr = np.array(bbox2d[0]-bbox1d[0])

            #the rotation/stretch matrix
            rad = rads[il]
            rot_mat = np.array([
                [math.cos(rad), math.sin(rad)*aspect],
                [-math.sin(rad)/aspect, math.cos(rad)]
            ])

            ##computing the offset vector of the rotated character
            drp = np.dot(dr,rot_mat)

            #setting final position and rotation:
            t.set_position(np.array([x,y])+drp)
            t.set_rotation(degs[il])

            t.set_va('center')
            t.set_ha('center')

            #updating rel_pos to right edge of character
            rel_pos += w-used