#!/usr/bin/env python
VERSION = '0.2 (2019-08-16)'

import zipfile

def execute(file):
    import subprocess, os, platform
    file = os.path.abspath(file)
    if platform.system() == 'Darwin':       # macOS
        subprocess.call(('open', file))
    elif platform.system() == 'Windows':    # Windows
        os.startfile(file)
    else:                                   # linux variants
        subprocess.call(('xdg-open', file))

def read_header(filename, archive=None):

    import re
    import os
    re_crnsdata = re.compile(r'^\d+\,')
    re_columns = re.compile(r'/+(RecordNum.+)')
    re_columns2 = re.compile(r'/+(GpsUTC.+)')
    data_columns = []

    with archive.open(filename,'r') if archive else open(filename) as file:
        for line in file:
            if isinstance(line, bytes):
                line = line.decode(errors='ignore')
                
            if re_crnsdata.search(line):
                break
            else:
                match_columns = re_columns.search(line)
                if match_columns:
                    data_columns = match_columns.group(1)
                    continue
                match_columns2 = re_columns2.search(line)
                if match_columns2:
                    data_columns += ',' + match_columns2.group(1)
                    continue

    if len(data_columns)<=0:
        gui.Popup('Input file contains no headers (e.g., //RecordNum...)!', non_blocking=True)
        return(None)
    else:
        data_columns = re.sub(r'\s*,+\s*', ',', data_columns)
        data_columns = data_columns.split(',')
        return(data_columns)

ufzcolor = '#00508E'

from glob import glob
import sys
import PySimpleGUI as gui
import subprocess

tab_download = [
	[
        #gui.Checkbox('Check for new data', key='download', default=True)
        gui.Radio('Do not check for new data', group_id='source', key='source_None', default=True)
    ],
    [
        gui.Radio('Download from FTP source', group_id='source', key='source_FTP')
	],
	[
        gui.Frame('', [
        [
            gui.Text('Server:', size=(15, 1)),
            gui.Input(do_not_clear=True, key='FTP_server'),
        ],
        [
            gui.Text('Username:', size=(15, 1)),
            gui.Input(do_not_clear=True, key='FTP_user'),
        ],
        [
            gui.Text('Password:', size=(15, 1)),
            gui.Input(do_not_clear=True, key='FTP_pswd', password_char='*'),
        ],
        [	gui.Text('Remote file pattern:', size=(15, 1)),
            gui.Text('starts with:'),
            gui.Input('19', size=(10, 1), do_not_clear=True, key='FTP_prefix', tooltip='Prefix (file name starts with ...)'),
            gui.Text(', ends with:'),
            #gui.Text('*', size=(1, 1), justification='center', tooltip='any characters'),
            gui.Input('.RV1', size=(7, 1), do_not_clear=True, key='FTP_suffix', tooltip='Suffix (file name ends with ...)'),
            gui.Button('Test pattern', key='FTP_list_files'),
        ]]),
	],
	[
        gui.Radio('Download from SD card', group_id='source', key='source_SD')
	],
	[
        gui.Frame('', [
        [
            gui.Text('SD card path:', size=(15, 1)),
            gui.Input(do_not_clear=True, key='SD_path'),
        ],
        [	gui.Text('SD file pattern:', size=(15, 1)),
            gui.Text('starts with:'),
            gui.Input('19', size=(10, 1), do_not_clear=True, key='SD_prefix', tooltip='Prefix (file name starts with ...)'),
            gui.Text(', ends with:'),
            #gui.Text('*', size=(1, 1), justification='center', tooltip='any characters'),
            gui.Input('.RV1', size=(7, 1), do_not_clear=True, key='SD_suffix', tooltip='Suffix (file name ends with ...)'),
            gui.Button('Test pattern', key='SD_list_files'),
        ]])
	],
#	[
#        gui.Text('Local archive path:', size=(15, 1)),
#		gui.Input(do_not_clear=True, key='archive_path'),
#		gui.FolderBrowse(target='archive_path')
#	],
]

tab1_layout = [
	[	gui.Text('Input folder path:', size=(15, 1)),
		gui.Input(do_not_clear=True, key='path'),
		gui.FolderBrowse(target='path')
	],
	[	gui.Text('File pattern:', size=(15, 1)),
		gui.Text('starts with:'),
		gui.Input('19', size=(10, 1), do_not_clear=True, key='prefix', tooltip='Prefix (file name starts with ...)'),
		gui.Text(', ends with:'),
		#gui.Text('*', size=(1, 1), justification='center', tooltip='any characters'),
		gui.Input('.RV1', size=(7, 1), do_not_clear=True, key='suffix', tooltip='Suffix (file name ends with ...)'),
		gui.Button('Test pattern', key='list_input_files'),
	],
	[
		gui.Button('Read data columns', key='read_columns'),
		gui.T('    Select columns in the list and attribute them to certain variables:')
	],
	[
        gui.Listbox([], key='column_list', size=(15, 15), select_mode='extended'),
        gui.Column([
            [gui.Button('-> epithermal neutrons:', key='set_ep_neutrons', size=(17,1)), gui.Input('N1Cts, N2Cts', key='neutron_columns')],
            [gui.Button('-> thermal neutrons:    ', key='set_th_neutrons', size=(17,1)), gui.Input('', key='thermal_neutron_columns')],
            [gui.Button('-> time resolution:      ', key='set_resolution', size=(17,1)), gui.Input('N1ETsec', key='resolution_column', size=(31,1)),
             gui.Text('default:', size=(5,1)), gui.Input('', size=(4,1), key='resolution_default')],
            [gui.Button('-> air pressure:      ', key='set_p', size=(17,1)), gui.Input('PTB110_mb', key='pressure_column')],
            [gui.Button('-> air humidity:      ', key='set_h', size=(17,1)), gui.Input('RH_CS215', key='humidity_column')],
            [gui.Button('-> air temperature:   ', key='set_T', size=(17,1)), gui.Input('T_CS215', key='temperature_column')],
            [gui.Button('-> latitude:         ', key='set_lat', size=(17,1)), gui.Input('LatDec', key='latitude_column')],
            [gui.Button('-> longitude:        ', key='set_lon', size=(17,1)), gui.Input('LongDec', key='longitude_column')],
            ])
	]
]

tab2_layout = [
    [   gui.Column([
        [   gui.Frame('Period selection', [
            [
                gui.Text('Start:', size=(5, 1)),
                gui.Input('', do_not_clear=True, size=(20,1), key='start'),
                gui.CalendarButton('Calendar', target='start')
            ],
            [
                gui.Text('End:', size=(5, 1)),
                gui.Input('', do_not_clear=True, size=(20,1), key='end'),
                gui.CalendarButton('Calendar', target='end'),
            ]])
        ],
        [   gui.Frame('Smoothing', [
            [
                gui.Text('Aggregate to fixed time steps:', size=(22, 1)),
                gui.Input('1', do_not_clear=True, size=(3,1), key='aggregate_num', tooltip='leave blank for no aggregation'),
                gui.DropDown(['','min','hour','day','week','month','year'], key='aggregate_unit', tooltip='leave blank for no aggregation'),
            ],
            [
                gui.Text('Moving (rolling) average over:', size=(22, 1)),
                gui.Input('1', do_not_clear=True, size=(3,1), key='smooth', tooltip='leave blank for no moving avg'),
                gui.Text('time steps'),
            ],
            [
                gui.Text('Spatial average over nearest', size=(22, 1)),
                gui.Input('1', do_not_clear=True, size=(3,1), key='number_locations', tooltip='leave blank for no spatial smoothing'),
                gui.Text('locations'),
            ]])
            
        ]
        ]),
        gui.Column([
        [   gui.Frame('Valid data ranges', [
            [
                gui.T('Air pressure:', size=(15, 1)),
                gui.Input('1', size=(5,1), key='pressure_min'),
                gui.T('<', size=(1,1)),
                gui.Input('1200', size=(5,1), key='pressure_max'),
                gui.T('hPa')
            ],
            [
                gui.T('Air humidity:', size=(15, 1)),
                gui.Input('0', size=(5,1), key='humidity_min'),
                gui.T('<', size=(1,1)),
                gui.Input('100', size=(5,1), key='humidity_max'),
                gui.T('%')
            ],
            [
                gui.T('Air Temperature:', size=(15, 1)),
                gui.Input('-60', size=(5,1), key='temperature_min'),
                gui.T('<', size=(1,1)),
                gui.Input('60', size=(5,1), key='temperature_max'),
                gui.T('C')
            ],
            [
                gui.T('Corrected neutron:', size=(15, 1)),
                gui.Input('2000', size=(5,1), key='neutron_min'),
                gui.T('<', size=(1,1)),
                gui.Input('17000', size=(5,1), key='neutron_max'),
                gui.T('cph')
            ],
            [
                gui.T('soil moisture:', size=(15, 1)),
                gui.Input('0', size=(5,1), key='sm_min'),
                gui.T('<', size=(1,1)),
                gui.Input('1', size=(5,1), key='sm_max'),
                gui.T('g/g')
            ],
            ])
        ],
        [   gui.Frame('Missing data', [
            [
                gui.T('Invalid data:', size=(10, 1)),
                gui.DropDown(['drop lines', 'replace by NaN'], key='invalid_data')
            ],
            [
                gui.Checkbox('Interpolate neutrons over NaNs', key='interpolate_neutrons', default=False),
            ],
            [
                gui.T('Replace missing pressure by:', size=(24,1)),
                gui.Input('', size=(5,1), key='missing_pressure'),
                gui.T('hPa'),
            ],
            [
                gui.T('Replace missing humidity by:', size=(24,1)),
                gui.Input('', size=(5,1), key='missing_humidity'),
                gui.T('%'),
            ],
            [
                gui.T('Replace missing temperature by:', size=(24,1)),
                gui.Input('', size=(5,1), key='missing_temperature'),
                gui.T('C'),
            ]
            ])
        ]
        ])
    ]
]
tab3_layout = [
    [	gui.Text('Output folder path:', size=(15, 1)),
		gui.Input(do_not_clear=True, key='out_path'),
		gui.FolderBrowse(target='out_path')
	],
	[
        gui.Text('File basename:', size=(15, 1)),
		gui.Input(do_not_clear=True, key='out_basename'),
	],
	[
        gui.Frame('CSV file', [
            [gui.Checkbox('Make .csv file (all columns)', key='make_CSV', default=True)],
            [gui.Text('Datetime format:', size=(15, 1)), gui.Input('Y-m-d H:M:S', do_not_clear=True, size=(12,1), key='CSV_datetime_format', tooltip='Y=year, m=month, d=day, H=hour, M=minute, S=second')],
            [gui.Text('Float format:', size=(15, 1)), gui.Input('11.5f', do_not_clear=True, size=(12,1), key='CSV_float_format', tooltip='e.g., .xf, where x is the decimal precision')],
            [gui.Text('Decimal char:', size=(15, 1)), gui.DropDown(['.',','], key='CSV_decimal')],
            [gui.Text('Column separation:', size=(15, 1)), gui.DropDown([',',';',' ',r'\t','|'], key='CSV_column_sep')],
            [gui.Text('Missing values:', size=(15, 1)), gui.DropDown(['NaN','NA','NULL','-9999',''], key='CSV_NaN')],
            [gui.Text('')]
        ]),
        gui.Frame('KML file', [
            [gui.Checkbox('Make .kml file for neutrons (cph)', key='make_KML_neutrons', default=True)],
            [
                gui.T('Value range:', size=(10, 1)),
                gui.Input('', size=(5,1), key='KML_neutrons_min'),
                gui.T('<', size=(1,1)),
                gui.Input('', size=(5,1), key='KML_neutrons_max')
            ],
            [gui.Checkbox('Make .kml file for soil moisture (%)', key='make_KML_sm', default=True)],
            [
                gui.T('Value range:', size=(10, 1)),
                gui.Input('', size=(5,1), key='KML_sm_min'),
                gui.T('<', size=(1,1)),
                gui.Input('', size=(5,1), key='KML_sm_max')
            ],
            [gui.Checkbox('Make .kml file for:', key='make_KML_other', default=False),
             gui.Input('', key='KML_other_column',size=(10,1))],
            [
                gui.T('Value range:', size=(10, 1)),
                gui.Input('', size=(5,1), key='KML_other_min'),
                gui.T('<', size=(1,1)),
                gui.Input('', size=(5,1), key='KML_other_max')
            ],
            [
                gui.T('Float format:', size=(10, 1)),
                gui.Input('%.1f', size=(5,1), key='KML_other_format'),
                gui.Checkbox('Reverse colors', key='KML_other_reverse', default=False)
            ]
        ])
	],
	[
        gui.Frame('PDF file', [
            [gui.Checkbox('Make .pdf file (plots N, p, rh, T, ah, sm)', key='make_PDF', default=True)],
            [gui.Text('Plot more columns:', size=(15, 1)), gui.Input('LatDec, LongDec, Alt', size=(55,1), key='PDF_plots')],
            [gui.Text('Time axis format:', size=(15, 1)), gui.Input('H:M', key='PDF_time_format', size=(12,1), tooltip=r'Y=year, m=month, b=month_name_short, d=day, H=hour, M=minute, S=second, \n=line_break')],
        ])
	],
	[gui.Text('')]
	]

corr_layout = [
    [
        gui.Text('Column for corrected neutrons:', size=(25, 1)),
		gui.Input('N', do_not_clear=True, size=(10,1), key='new_neutron_column'),
	],
    [   gui.Column(
        [
            [
                gui.Frame('Humidity correction',
                [
                    [ gui.T('Column abs. humidity:', size=(18,1)), gui.Input('ah', key='new_humidity_column', size=(8,1)) ],
                    [ gui.T('Method:', size=(18,1)), gui.DropDown(['None','Rosolem et al. (2013)'], key='humidity_method') ],
                    [ gui.T('Reference ah_ref:', size=(18,1)), gui.Input('0', key='humidity_ref', size=(8,1)), gui.T('g/m3') ],
                    [ gui.T('Parameter alpha:', size=(18,1)), gui.Input('0.0054', key='alpha', size=(8,1)) ],
                ])
            ],
            [
                gui.Frame('Pressure correction',
                [
                    [ gui.T('Method:', size=(18,1)), gui.DropDown(['None', 'Zreda et al. (2012)', 'Desilets et al. (2006)'], key='pressure_method') ],
                    [ gui.T('Reference p_ref:', size=(18,1)), gui.Input('1013.25', key='pressure_ref', size=(8,1)), gui.T('hPa') ],
                    [ gui.T('Attenuation length beta:', size=(18,1)), gui.Input('131.6', key='beta', size=(8,1)) ],
                ])
            ]
        ]),
        gui.Column(
        [
            [
                gui.Frame('Incoming correction',
                [
                    [ gui.T('Column NM data:', size=(18,1)), gui.Input('NM', key='new_incoming_column', size=(8,1)) ],
                    [ gui.T('Method:', size=(18,1)), gui.DropDown(['None', 'Zreda et al. (2012)', 'Hawdon et al. (2014)', 'PARMA'], key='incoming_method') ],
                    [ gui.T('Reference NM_ref:', size=(18,1)), gui.Input('150', key='incoming_ref', size=(8,1)), gui.T('cps') ],
                    [ gui.T('Rescaling factor gamma:', size=(18,1)), gui.Input('1', key='gamma', size=(8,1)) ],
                    [ gui.T('Location (lat,lon):', size=(18,1)), gui.Input('51', key='sensor_lat', size=(8,1)), gui.T(','), gui.Input('11', key='sensor_lon', size=(8,1)) ],
                ])
            ],
            [
                gui.Frame('Neutron Monitor data',
                [
                    [ gui.Checkbox('Download data automatically', default=True, key='NM_auto_download')],
                    [ gui.T('NM station:', size=(18,1)), gui.DropDown(['AATA','AATB','APTY','ARNM','ATHN','BKSN','CALG','CALM','DJON','DOMB','DOMC','DRBS','ESOI','FSMT','HRMS','INVK','IRK2','IRK3','IRKT','JBGO','JUNG','JUNG1','KERG','KIEL','KIEL2','LMKS','MCRL','MGDN','MOSC','MRNY','MWSN','MXCO','NAIN','NANM','NEU3','NEWK','NRLK','NVBK','OULU','PSNM','PTFM','PWNK','ROME','SANB','SNAE','SOPB','SOPO','TERA','THUL','TIBT','TSMB','TXBY','YKTK'], default_value='JUNG', key='NM_station')],
                    [ gui.T('NM resolution:', size=(18,1)), gui.DropDown(['1min', '10min', '1hour', '1day'], default_value='1min', key='NM_resolution')],
                    [
                        gui.Text('NM folder:', size=(8, 1)),
                        gui.Input(do_not_clear=True, key='NM_path', size=(24, 1)),
                        gui.FolderBrowse(target='NM_path')
                    ]
                ])
            ],
        ])],
]

conv_layout = [
    [
        gui.Text('Column for soil moisture:', size=(18, 1)),
		gui.Input('moisture', do_not_clear=True, size=(10,1), key='new_moisture_column'),
	],
	[
        gui.Column(
        [
            [
                gui.Frame('Soil properties',
                [
                    [
                        gui.Text('Bulk density:', size=(15, 1)),
                        gui.Input('1', do_not_clear=True, size=(7,1), key='bulk_density'),
                        gui.Text('kg/m3')
                    ],
                    [
                        gui.Text('Soil organic carbon:', size=(15, 1)),
                        gui.Input('0.02', do_not_clear=True, size=(7,1), key='soil_org_carbon'),
                        gui.Text('g/g')
                    ],
                    [
                        gui.Text('Lattice water:', size=(15, 1)),
                        gui.Input('0.02', do_not_clear=True, size=(7,1), key='lattice_water'),
                        gui.Text('g/g')
                    ]
                ])
            ],
            [
                gui.Frame('Vegetation',
                [
                    [
                        gui.T('Method:', size=(15,1)),
                        gui.DropDown(['None', 'Baatz et al. (2014)', 'Franz et al. (2014)'], key='veg_method')
                    ],
                    [
                        gui.Text('Biomass:', size=(15, 1)),
                        gui.Input('0', do_not_clear=True, size=(7,1), key='biomass'),
                        gui.Text('kg/m2'),
                    ],
                ])
            ],
            [
                gui.Frame('Snow',
                [
                    [
                        gui.T('Method:', size=(15,1)),
                        gui.DropDown(['None', 'Sigouin et al. (2016)', 'Desilets et al. (2017)', 'Schattan et al. (2018)'], key='snow_method')
                    ],
                    [
                        gui.Text('Snow water equiv.:', size=(15, 1)),
                        gui.Input('0', do_not_clear=True, size=(7,1), key='SWE'),
                        gui.Text('mm'),
                    ],
                ])
            ],
            [
                gui.Frame('Road',
                [
                    [
                        gui.T('Method:', size=(15,1)),
                        gui.DropDown(['None', 'Schroen et al. (2018)'], key='road_method')
                    ],
                    [
                        gui.Text('Road moisture:', size=(15, 1)),
                        gui.Input('0', do_not_clear=True, size=(7,1), key='road_moisture'),
                        gui.Text('g/g'),
                    ],
                    [
                        gui.Text('Road width:', size=(15, 1)),
                        gui.Input('0', do_not_clear=True, size=(7,1), key='road_width'),
                        gui.Text('m'),
                    ],
                ])
            ],
        ]),
        gui.Column(
        [
            [
                gui.Frame('Neutrons to Soil Moisture',
                [
                    [
                        gui.T('Method:', size=(13,1)),
                        gui.DropDown(['None','Desilets et al. (2010)', 'Köhli et al. (2015)'], key='N2sm_method')
                    ],
                    [
                        gui.Text('a0, a1, a2:', size=(13, 1)),
                        gui.Input('0.0808', do_not_clear=True, size=(7,1), key='a0'),
                        gui.Input('0.372', do_not_clear=True, size=(7,1), key='a1'),
                        gui.Input('0.115', do_not_clear=True, size=(7,1), key='a2'),
                    ],
                    [
                        gui.Text('N0:', size=(13, 1)),
                        gui.Input('11447', do_not_clear=True, size=(7,1), key='N0'),
                        gui.Text('cph')
                    ],
                ])
            ],
            [
                gui.Frame('Calibration',
                [
                    [
                        gui.Checkbox('Calibrate N0 on run-time', size=(18,1), key='calibrate_N0'),
                    ],
                    [
                        gui.Text('Campaign start:', size=(13, 1)),
                        gui.Input('', do_not_clear=True, size=(20,1), key='campaign_start'),
                        gui.CalendarButton('Calendar', target='campaign_start')
                    ],
                    [
                        gui.Text('Campaign end:', size=(13, 1)),
                        gui.Input('', do_not_clear=True, size=(20,1), key='campaign_end'),
                        gui.CalendarButton('Calendar', target='campaign_end'),
                    ],
                    [
                        gui.Text('Measured soil moisture:', size=(17, 1)),
                        gui.Input('0.02', do_not_clear=True, size=(7,1), key='measured_sm'),
                        gui.Text('m3/m3')
                    ],
                ])
            ],
        ])
	],
]

tab4_layout = [
    [gui.Output(size=(108,20), key='output')], #, font='Courier 10', background_color='SystemButtonFace'
    [
        gui.T('View output:', size=(10,1), justification='right', key='run_text'),
        gui.Button('CSV', key='view_CSV', disabled=True),
        gui.Button('PDF', key='view_PDF', disabled=True),
        gui.Button('KML neutrons', key='view_KML_neutrons', disabled=True),
        gui.Button('KML moisture', key='view_KML_sm', disabled=True),
        gui.Button('KML other', key='view_KML_any', disabled=True)
    ]
]
tab5_layout = [
    [
        gui.Column([
            [gui.Image('docs/instant-pasdy.png')],
            [gui.T('')],
            [gui.Button('Read the manual'), gui.Button('Website')]
        ]),
        gui.Column([
            [gui.T("Instant-Pasdy is a code-free variant of Pasdy\n-- Processing and Analysis of Sensor Data in pYthon.")],
            [gui.T("Version %s\nfeat. CoRNish Pasdy (COsmic-Ray Neutron flavored)." % VERSION)],
            [gui.Image('docs/UFZ.png'), gui.Image('docs/CosmicSense.png')],
            [gui.T("Developed by the Helmholtz Centre for Environmental Research (UFZ).\nSupported by the Deutsche Forschungsgemeinschaft (DFG),\nResearch Unit 'Cosmic Sense' (FOR-2694).")],
            [gui.T("Written by Martin Schrön. Contact: martin.schroen@ufz.de\nThanks to: Mandy Kasner, Jannis Jakobi,\nDarin Desilets, and Rafael Rosolem.")]
        ])
    ]
]

layout = [
    [
        gui.T('Config file:'),
        gui.InputText(key='configfile'),
        gui.InputText(key='_configfile', change_submits=True, enable_events=True, visible=False),
        gui.FileBrowse(key='config_browse', target='_configfile'),
        gui.Button('Save & Run!', key='run'),
        gui.ProgressBar(100, orientation='h', size=(12, 20), key='progressbar', bar_color=(ufzcolor, 'SystemButtonFace')),
        gui.T('', size=(11,1), key='status'),
        #gui.Exit()
    ],
    [
        gui.TabGroup([[
            gui.Tab('0. Update', tab_download),
            gui.Tab('1. Input', tab1_layout),
            gui.Tab('2. Cleaning', tab2_layout),
            gui.Tab('3. Correction', corr_layout),
            gui.Tab('4. Conversion', conv_layout),
            gui.Tab('5. Output', tab3_layout),
            gui.Tab('6. Results', tab4_layout),
            gui.Tab('Info', tab5_layout)
        ]])
    ]
]

#You cannot change button colors for Macs.
#'browse'         'extended'         'multiple'         'single'
# The listbox offers four different selection modes through the selectmode option. These are SINGLE (just a single choice), BROWSE (same, but the selection can be moved using the mouse), MULTIPLE (multiple item can be choosen, by clicking at them one at a time), or EXTENDED (multiple ranges of items can be chosen, using the Shift and Control keyboard modifiers). The default is BROWSE. Use MULTIPLE to get "checklist" behavior, and EXTENDED when the user would usually pick only one item, but sometimes would like to select one or more ranges of items.


import instantPASDy
progress = 0

# Overwrite print() with flush, i.e., no buffer
_orig_print = print
def myprint(*args, **kwargs):
    _orig_print(*args, **kwargs)
    if not args[0].startswith(' '):
        global progress
        progress += 3
        if progress >= 100: progress = 99
        window.Element('progressbar').UpdateBar(progress)
        window.Element('status').Update(value=args[0].strip().split(' ', 1)[0])
    window.Refresh()
instantPASDy.print = myprint

window = gui.Window('Instant-Pasdy GUI', button_color=('white', ufzcolor), progress_bar_color=('white', ufzcolor)).Layout(layout).Finalize()
#  you do not need a Finalize call unless you plan on doing some operation on your window prior to calling Read.

import configparser
from configobj import ConfigObj # better alternative to configparser: preserves order and comments
import os
import re

def yesno2bool(s):
    s = s.lower()
    if s == 'yes' or s == 'y':
        return(True)
    else:
        return(False)

def bool2yesno(boo):
    if boo:
        return('yes')
    else:
        return('no')

output_basename = ''

while True:
    event, values = window.Read()
    #print(event, values)
    
    if event in (None, 'Exit'):
        break
    if event == '_configfile':
        # Load the config into the GUI
        #config = configparser.ConfigParser()
        window.Element('configfile').Update(window.Element('_configfile').Get())
        configfile = window.Element('configfile').Get()
        if configfile == '': configfile = 'config/config.cfg'
        if os.path.isfile(configfile):
            #temp = config.read(configfile)
            config = ConfigObj(configfile, encoding="cp850")
        else:
            #gui.Popup('Config file required! Either specify as argument or provide config.cfg')
            # Do nothing, because an unknown filename could be used to save a new config file.
            pass

        # clean
        if os.path.isdir(config['input']['path']):
            config['input']['path'] = os.path.join(config['input']['path'], '') # add trailing slash
        config['output']['out_path'] = os.path.join(config['output']['out_path'], '') # add trailing slash
        for section in config.sections:
            for k in config[section]:
                v = config[section][k]
                if k in window.AllKeysDict:
                    element = window.Element(k, silent_on_error=True)
                    if element.Type == 'input':
                        if isinstance(v, list):
                            element.Update(', '.join(v))
                        else:
                            element.Update(v)

                    elif element.Type == 'checkbox':
                        element.Update(yesno2bool(v))

                    elif element.Type == 'combo':
                        if v in element.Values:
                            element.Update(v)
                        else:
                            element.Update('None')
                else:
                    # Set k_min and k_max values for k_range
                    if k.endswith('_range'):
                        if not isinstance(v, list): v = ['','']
                        try:
                            window.Element(k[:k.index('_range')]+'_min').Update(v[0])
                            window.Element(k[:k.index('_range')]+'_max').Update(v[1])
                        except:
                            print('ERROR: cannot find input elements _min and _max for keyword %s!' % k)


                    elif k == 'aggregate':
                        if v:
                            v_bundle = re.search('^(\d+)([a-z]+)', v, re.IGNORECASE)
                            v = [v_bundle.group(1), v_bundle.group(2)]
                        else:
                            v = ['','']

                        try:
                            window.Element(k+'_num').Update(v[0])
                            window.Element(k+'_unit').Update(v[1])
                        except:
                            pass

                    elif k == 'data_source':
                        try:
                           window.Element('source_'+v).Update(True)
                        except:
                            pass

                    else:
                        print('ERROR: something is wrong with keyword %s!' % k)

#                if window.Element(k, silent_on_error=True):
#                    print('%s exists and is of type %s.' % (k, window.Element(k).Type))
#                else:
#                    print('ERROR:%s absent' % k)

#                try:
#                    window.Element(k).Update(config[section][k])
            # element.Type == ELEM_TYPE_INPUT_CHECKBOX:ELEM_TYPE_INPUT_TEXT

#elif element.Type == 'checkbox':
#                    value = element.TKIntVar.get()
#value = (value != 0)
#elif element.Type == 'combo':
#value = element.TKStringVar.get()
#            if element.Type == 'text':
#                    try:
#                        value = element.TKStringVar.get()
#                    except:
#value = ''
#            if element.BType == BUTTON_TYPE_CALENDAR_CHOOSER: #BUTTON_TYPE_CALENDAR_CHOOSER = 30
#                        try:
#                            value = element.TKCal.selection
#                        except:
#value = None
# elif element.Type == ELEM_TYPE_INPUT_COMBO:
#value = element.TKStringVar.get()
#            window.Element('neutron_columns').Update(value=', '.join(config['input']['neutron_columns']))
#            window.Element('thermal_neutron_columns').Update(value=', '.join(config['input']['thermal_neutron_columns']))
#            window.Element('pressure_min').Update(value=config['clean']['pressure_range'][0])
#            window.Element('pressure_max').Update(value=config['clean']['pressure_range'][1])
#            window.Element('humidity_min').Update(value=config['clean']['humidity_range'][0])
#            window.Element('humidity_max').Update(value=config['clean']['humidity_range'][1])
#            window.Element('temperature_min').Update(value=config['clean']['temperature_range'][0])
#            window.Element('temperature_max').Update(value=config['clean']['temperature_range'][1])
#            window.Element('neutron_min').Update(value=config['clean']['neutron_range'][0])
#            window.Element('neutron_max').Update(value=config['clean']['neutron_range'][1])
#            window.Element('sm_min').Update(value=config['clean']['sm_range'][0])
#            window.Element('sm_max').Update(value=config['clean']['sm_range'][1])
#            window.Element('interpolate_neutrons').Update(value=yesno2bool(config['clean']['interpolate_neutrons']))


        #for section in config.sections():
        #    window.Fill(config._sections[section])

    if event == 'run':
        # First, save the config
        for section in config.sections:
            for k in config[section]:
                if k in window.AllKeysDict:
                    element = window.Element(k, silent_on_error=True)
                    if element.Type == 'input':
                        v = element.Get()
                        if ',' in v:
                            v = [x.strip() for x in v.split(',')]

                    elif element.Type == 'checkbox':
                        v = bool2yesno(element.Get())

                    elif element.Type == 'combo':
                        v = element.TKStringVar.get()
                else:
                    # Set k_min and k_max values for k_range
                    if k.endswith('_range'):
                        try:
                            v = [window.Element(k[:k.index('_range')]+'_min').Get(), window.Element(k[:k.index('_range')]+'_max').Get()]
                            if v[0] == '': v = ''
                        except:
                            print('ERROR: cannot find input elements _min and _max for keyword %s!' % k)

                    elif k == 'aggregate':
                        try:
                            if window.Element(k+'_num').Get() == '':
                                v = ''
                            else:
                                v = window.Element(k+'_num').Get() + window.Element(k+'_unit').TKStringVar.get()
                        except:
                            pass

                    elif k == 'data_source':
                        for k_radio in ['None', 'FTP', 'SD']:
                            if window.Element('source_'+k_radio).Get():
                                v = k_radio
                                break

                    else:
                        print('ERROR: something is wrong with keyword %s!' % k)

                config[section][k] = v

        config.filename = window.Element('configfile').Get()
        config.write_empty_values = True
        config.write()
        print('Saved file %s' % window.Element('configfile').Get())

        output_basename = os.path.join(values['out_path'],'') + values['out_basename']
        
#        for section in config.sections():
#            for (k, v) in config.items(section):
                #try:
                #    window.Element(k, silent_on_error=True).Update(value=v)
                #except:
                #    print('Did not work for ' + k)
                #    pass

        # reset elements
        progress = 0
        window.Element('progressbar').UpdateBar(0)
        for view_button in ['view_CSV', 'view_PDF', 'view_KML_neutrons', 'view_KML_sm', 'view_KML_any']:
            window.Element(view_button).Update(disabled=True)

        # run
        data = instantPASDy.main(window.Element('configfile').Get())
        start_str = data.index.min().strftime('%Y%m%d')
        end_str = data.index.max().strftime('%Y%m%d')
        end_str = '-'+end_str if start_str != end_str else ''
        output_basename += '-' + start_str + end_str

        # finish
        window.Element('progressbar').UpdateBar(100)
        window.Element('status').Update(value = 'Finished.')

        # show view buttons
        for view in ['CSV', 'PDF', 'KML_neutrons', 'KML_sm', 'KML_other']:
            if values['make_'+view]: window.Element('view_'+view).Update(disabled=False)

    elif event == 'launch':
        sp = subprocess.Popen([r"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", 'http://www.test.de'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    elif event == 'list_input_files':
    
        pattern = values['path']+'/'+values['prefix']+'*'+values['suffix']
        files = []
        if values['path'].endswith('.zip'):
            archive = zipfile.ZipFile(values['path'], 'r')
            files = [x for x in archive.namelist() if x.endswith(values['suffix']) and x.startswith(values['prefix'])]
        elif os.path.isdir(values['path']):
            files = glob(pattern)
        else:
            gui.Popup("Input path is neither a folder nor a zip archive.", title='Files matching "%s"' % pattern, non_blocking=True)
            break
            
        slist = []
        for filename in files:
            slist.append(filename)
        gui.PopupScrolled("\n".join(slist), title='Files matching "%s"' % pattern, non_blocking=True)

    elif event == 'SD_list_files':
        pattern = values['SD_path']+'/'+values['SD_prefix']+'*'+values['SD_suffix']
        slist = []
        for filename in glob(pattern):
            slist.append(os.path.basename(filename))
        gui.PopupScrolled("\n".join(slist), title='Files matching "%s"' % pattern, non_blocking=True)
        
    elif event == 'FTP_list_files':
        import ftplib
        remote_list = []
        try:
            remote = ftplib.FTP(values['FTP_server']) 
            remote.login(values['FTP_user'], values['FTP_pswd'])
            remote_list = list(filter(lambda item: item.startswith(values['FTP_prefix']), remote.nlst()))
            remote.close()
        except:
            gui.Popup('Cannot connect to the FTP server, bad credentials.')
            pass
            
        gui.PopupScrolled("\n".join(remote_list), title='Files matching "%s"' % pattern, non_blocking=True)
        
    elif event == 'read_columns':
        pattern = values['path']+'/'+values['prefix']+'*'+values['suffix']
        archive = None
        files = []
        if values['path'].endswith('.zip'):
            archive = zipfile.ZipFile(values['path'], 'r')
            files = [x for x in archive.namelist() if x.endswith(values['suffix']) and x.startswith(values['prefix'])]
        elif os.path.isdir(values['path']):
            files = glob(pattern)
        else:
            gui.Popup("Input path is neither a folder nor a zip archive.", title='Files matching "%s"' % pattern, non_blocking=True)
            break
            
        columns = read_header(files[0], archive) if files else []
        if not archive is None: archive.close()
        if not columns:
            gui.Popup('Cannot find any files matching "%s".\nPlease check input folder and file pattern.' % pattern)
        else:
            window.Element('column_list').Update(values=columns)
    
    elif event == 'set_ep_neutrons': window.Element('neutron_columns').Update(value=', '.join(values['column_list']))
    elif event == 'set_th_neutrons': window.Element('thermal_neutron_columns').Update(value=', '.join(values['column_list']))
    elif event == 'set_resolution':  window.Element('resolution_column').Update(value=', '.join(values['column_list']))
    elif event == 'set_p':           window.Element('pressure_column').Update(value=', '.join(values['column_list']))
    elif event == 'set_h':           window.Element('humidity_column').Update(value=', '.join(values['column_list']))
    elif event == 'set_T':           window.Element('temperature_column').Update(value=', '.join(values['column_list']))
    elif event == 'set_lat':         window.Element('latitude_column').Update(value=', '.join(values['column_list']))
    elif event == 'set_lon':         window.Element('longitude_column').Update(value=', '.join(values['column_list']))

    elif event == 'view_CSV':         execute(output_basename +'.csv')
    elif event == 'view_PDF':         execute(output_basename +'.pdf')
    elif event == 'view_KML_neutrons':execute(output_basename +'-'+values['new_neutron_column']+'.kml')
    elif event == 'view_KML_sm':      execute(output_basename +'-'+values['new_moisture_column']+'.kml')
    elif event == 'view_KML_any':     execute(output_basename +'-'+config['output']['KML_other_column']+'.kml')

window.Close()