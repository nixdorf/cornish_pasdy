#!/usr/bin/env python
VERSION = '0.31 (2020-07-08)'

def debug():
    import pdb
    pdb.set_trace()
    
# Suppress nasty future warnings about pandas datetime conersion
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
#
from datetime import datetime, timedelta
import pytz
import dateutil
from dateutil.relativedelta import *
import pandas as pd
# Future Warning: mpl needs to be registered
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
#
pd.options.mode.chained_assignment = None
import sys
import requests
import re
import configparser
import os
import io
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.backends.backend_pdf
import simplekml
from glob import glob

#JJ packages used for calculating nearest neighbors
from heapq import nsmallest, nlargest
from scipy import stats
from math import sin, cos, sqrt, atan2, radians
import itertools
from sklearn.neighbors import KDTree

#import necessaries for roverweb library
from roverweb import weather,soilgrid,geometry,osm
import geopandas as gpd              
# own functions
from functions import *

# Overwrite print() with flush, i.e., no buffer
_orig_print = print
def print(*args, **kwargs):
    _orig_print(*args, flush=True, **kwargs)

def cprintf(s, fmt=''):
    os.system("") # makes it work also in the Win10 Console
    a = ''
    if fmt=='black':     a='\033[30m'
    if fmt=='red':       a='\033[31m'
    if fmt=='green':     a='\033[32m'
    if fmt=='yellow':    a='\033[33m'
    if fmt=='blue':      a='\033[34m'
    if fmt=='magenta':   a='\033[35m'
    if fmt=='cyan':      a='\033[36m'
    if fmt=='white':     a='\033[37m'
    if fmt=='underline': a='\033[4m'
    return(a + s + '\033[0m')
    

def report_N(data, column, correction='', units='cph'):
    s = 'i  %-30s  %5.0f +/- %5.0f %s' % (column, data[column].mean(), data[column+'_err'].mean(), units)
    if correction != '':
        if isinstance(correction, list):
            q = 1
            for c in correction:
                q *= data[c]
        elif correction in data.columns:
            q = data[correction]
            
        s += ' (%+.0f%%)' % (100*(1-q.mean()))
    print(cprintf(s, 'cyan'))

def report_SM(data, column, correction='', units='%'):
    s = 'i  %-30s  %3.0f %2.0f+%2.0f %s' % (column, data[column].mean()*100, data[column+'_err_low'].mean()*100, data[column+'_err_upp'].mean()*100, units)
    if correction != '':
        if isinstance(correction, list):
            q = 1
            for c in correction:
                q *= data[c]
        elif correction in data.columns:
            q = data[correction]
            
        s += ' (%+.0f%%)' % (100*(1-q.mean()))
        
    print(cprintf(s, 'cyan'))



#JJ recalculate strange coordinates to proper decimals 
# argument is a pandas.DataFrame(columns=[number, direction])
# remember that C.iloc[:,i] is the i^th column of C

def deg100min2dec(C):
    # Convert
    x = C.iloc[:,0] / 100
    deg = np.floor(x)
    dec = deg + (x - deg)/0.6
    # Negate if cardinal direction is south or west
    dec *= C.iloc[:,1].map({'N': 1, 'S': -1, 'E': 1, 'W': -1})
    return(dec)
       
       
#JJ use x nearest neighbors for averaging
## find k closest lat/lon to every lat/lon coordinates
def distance(pointA, pointB):
    return sqrt((pointB[0] - pointA[0]) ** 2 + (pointB[1] - pointA[1]) ** 2)

import zipfile
import os


def distanceinkm(xstand,ystand,xfor,yfor): # approximate radius of earth in km
   
    R = 6373.0
    lat1 = radians(xstand)
    lon1 = radians(ystand)
    lat2 = radians(xfor)
    lon2 = radians(yfor)
    
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return(distance)

def Neighbours(lat, lon, numpoints, data):
    numpoints = numpoints+1 # points itself included - therefore add one to get desired output
    points = np.vstack((lat, lon)).T # create numpy array from coordinates
    
    kdt = KDTree(points, leaf_size=30, metric='euclidean') # build tree with euclidean distances
    neighborpoints = kdt.query(points, k=numpoints, return_distance=False) # find the closest k points  
        
    # calculate maximal distance within the k clostest coordinates
    neighbordist = []
    for i in range(len(points)):
        furthest = np.array(nlargest(1, points[neighborpoints[i]], lambda p : distance(p, (points[i])))) # find the coordinates for the k closest points
        neighbordist.append(distanceinkm(points[i][0],points[i][1], furthest[0][0],furthest[0][1])*1000)
        
    return neighbordist, neighborpoints

    #neighborpoints = [] # create empty list for all results
    
    #for i in range(len(points)): ## iterate for all points
    #    nearest = np.array(nsmallest(numpoints, points, lambda p : distance(p, (points[i])))) # find the coordinates for the k closest points
    #    print(nearest)
    #    index = [] # create empty list for the index of the positions
               
    #    for m in range(len(nearest)): 
    #        idx, rest = np.where(points==nearest[m]) # find the rows of these closest k coordinates
    #        idx = stats.mode(idx) # calculate the mode - necessary because sometimes several outputs from above
    #        index.append(idx.mode.tolist()) # create list from the k rows found
                
    #    neighborpoints.append(index) # create list of k closest locations for all locations
    #for i in range(len(neighborpoints)):
    #    neighborpoints[i] = list(itertools.chain(*neighborpoints[i])) # reduce for one nested list
    
   
import sys
import shutil

def progressbar(index, total, length=None, title='Please wait', prefix='', end=False):
    if length is None:
        length = shutil.get_terminal_size()[0]-80
    percent_done = round((index+1)/total*100, 1)
    done = round(percent_done/(100/length))
    togo = length-done

    done_str = '>' * int(done) # '█'
    togo_str = ' ' * int(togo)

    if end: prefix = ' '*60
      
    print('| %-11s %s%s %3.0f%% %s' % (title, done_str, togo_str, percent_done, prefix), end='\r')
    sys.stdout.flush()

    if end: print('')      


def download(archive_file='', source='FTP', server=None, user=None, pswd=None,
             ftp_prefix='', ftp_suffix='', sd_path='', sd_prefix='', sd_suffix=''):

        # if a folder was given
        if not archive_file.endswith('zip'):
            archive_file = os.path.join(archive_file,'') + 'archive.zip'
            
        # Check what has already been downloaded
        archive = zipfile.ZipFile(archive_file, 'a', zipfile.ZIP_DEFLATED)
        archive_list = archive.namelist()
        archive_size = os.stat(archive_file).st_size /1024/1024
        print("i %5s files were already downloaded (%.2f MB)." % (len(archive_list), archive_size))

        if source=='FTP':
            # Select new files from FTP
            import ftplib
            try:
                remote = ftplib.FTP(server) 
                remote.login(user, pswd)
                remote_list = list(filter(lambda item: item.startswith(ftp_prefix), remote.nlst()))
                print("i %5s files found on the FTP server." % len(remote_list))
            except ftplib.all_errors as e:
                print(cprintf('! FTP connection error: %s' % e, 'red'))
                exit()
                
        elif source=='SD':
            # Select new files from SD-Card
            #remote_list = list(filter(lambda item: item.endswith(suffix=''), os.listdir(sd_path)))
            remote_list = [os.path.basename(x) for x in glob(sd_path+'/'+sd_prefix+'*'+sd_suffix)]
            print("i %5s files found on the SD backup." % len(remote_list))

        elif source=='DMP':
            # Select new files from DMP
            remote_list = [os.path.basename(x) for x in glob(sd_path+'/'+sd_prefix+'*'+sd_suffix)]
            print("i %5s files found on the SD backup." % len(remote_list))

        
        else:
            print('! Method unknown.')
            return
        
        update_list = list(filter(lambda item: not item in archive_list, remote_list))
        print("i %5s new files selected for download." % len(update_list))

        if len(update_list) == 0: return
        
        # Download files and add to Zip
        from io import BytesIO
        i = 0
        for filename in update_list:
            i += 1
            #print("|  Archiving files %5s/%s: %s" % (i, len(update_list), filename))
            #progressbar(i-1, len(update_list), title='Archiving', prefix=filename)
            if source=='FTP':
                memory = BytesIO()
                remote.retrbinary("RETR " + filename, memory.write) # FTP->memory
                archive.writestr(filename, memory.getvalue())    # memory->archive
                
            elif source=='SD':
                archive.write(os.path.join(sd_path,'') + filename, filename)
        #progressbar(i-1, len(update_list), title='Archiving', prefix=filename, end=True)
        archive.close()
        if source=='FTP': remote.close()
        file_save_msg(archive_file, "Archive", end="\n")
        



# Neutron monitor class
class NM:

    # X = NM()
    def __init__(self, station=None, resolution=None, file=None, config=None, verbose=False):
        
        self.verbose = verbose
        self.data = None
        self.most_recent = False
        
        if config is None:
            self.__config_file = os.path.join(os.path.dirname(__file__), 'config/config.cfg')
            if self.verbose: print('> Reading NM config: ' + self.__config_file)
            self.config = configparser.ConfigParser()   
            temp = self.config.read(self.__config_file)
        else:
            self.config = config
        
        # Setup data directory
        if not os.path.exists(self.config['correction']['NM_path']):
            os.makedirs(self.config['correction']['NM_path'])
        
        if station is None:
            self.station = self.config['correction']['NM_station']
        else:
            self.station = station
            
        if resolution is None:
            self.resolution = self.config['correction']['NM_resolution']
        else:
            self.resolution = resolution
        
        if self.resolution == '1day':
            self.resolution_min = 1440
        elif self.resolution == '1hour':
            self.resolution_min = 60
        elif self.resolution == '10min':
            self.resolution_min = 10
        elif self.resolution == '1min':
            self.resolution_min = 1
        
        self.file = file
        #self.file = _filename(datetime.now().strftime('%Y%m%d%H%M%S'))

            
    def _filename(self, string=''):
        if string: string = '-' + str(string)
        return("%s%s-%s%s.dat" \
            % (self.config['correction']['NM_path'], self.station, self.resolution, string))
        
            
    def get(self, start=None, end=None):
        if end is None:
            end = datetime.now()
            self.most_recent = True
        
        # If the user set a filename, just read from it
        if not self.file is None:
            self._reader(file)
            self.data = self.data.tz_localize('UTC')
            return(self)
        
        if start is None:
            print('! I do not know which date to look up, please set a start date.')
            return(self)
        
        # Read data
        data = []
        if self.resolution == '1day':
            data.append(self._read(datetime(year=2006, month=1, day=1, tzinfo=pytz.UTC), datetime.datetime.utcnow().date(), self._filename()))
            
        elif self.resolution == '1hour':
            for y in range(end.year - start.year +1):
                data.append(self._read(datetime(year=start.year+y, month=1, day=1, tzinfo=pytz.UTC),
                                       datetime(year=start.year+y, month=12, day=31, hour=23, minute=59, tzinfo=pytz.UTC),
                                       self._filename(start.year+y)))
        
        elif self.resolution == '10min':
            delta = end - start
            for d in [start + relativedelta(months=+m) for m in range(0, delta.days//12+1)]:
                data.append(self._read(d.replace(day=1, hour=0, minute=0),
                                       d.replace(day=1, hour=23, minute=59, second=0) + relativedelta(months=+1) - timedelta(days=1),
                                       self._filename(d.strftime('%Y%m'))))
        
        elif self.resolution == '1min':
            delta = end - start
            for d in [start + timedelta(days=d) for d in range(0, delta.days+2)]:
                data.append(self._read(d.replace(hour=0, minute=0), d.replace(hour=23, minute=59, second=0), self._filename(d.strftime('%Y%m%d'))))
        
        self.data = pd.concat(data)
        #print("Available NM data from: %s to %s" % (self.data.index.min().strftime('%Y-%m-%d %H:%M'), self.data.index.max().strftime('%Y-%m-%d %H:%M')))
        return(self)
    
    # Read existing file or download
    def _read(self, start, end, file):
        
        is_tz_aware = start.tzinfo is not None and start.tzinfo.utcoffset(start) is not None
        if os.path.exists(file):
            
            data = self._reader(file, localize=is_tz_aware)
            
            now = datetime.now(pytz.utc)
            if end > now: self.most_recent = True 
            if start > now: return(None) # None when looking for a future day
            
            #print(data.index)
            if data.index.min() <= start and (data.index.max() + timedelta(minutes=self.resolution_min) >= end or self.most_recent):
                if not is_tz_aware:
                    data = data.tz_localize('UTC')
                return(data)
            else:
                print("\n! Data period is not covering the requested period.")
                print("!      Data from: %s to %s" % (data.index.min().strftime('%Y-%m-%d %H:%M'), data.index.max().strftime('%Y-%m-%d %H:%M')))
                print("! Requested from: %s to %s" % (start.strftime('%Y-%m-%d %H:%M'), end.strftime('%Y-%m-%d %H:%M')))
        
        self._download(start, end, file)
        return(self._reader(file))
            
                
    def _reader(self, file, localize=True):
        print('.', end='')
        data = pd.read_csv(file, index_col=0, header=None, sep=";", names=['time','NM'],
                           parse_dates=True, infer_datetime_format=True)
        if localize: 
            data = data.tz_localize('UTC')
            
        #print(data.index.dtype)
        return(data)
        
        
    
    def _download(self, start, end, file, force=True):
    
        force_str = '&force=1' if force else ''
        url = "http://www.nmdb.eu/nest/draw_graph.php?wget=1&stations[]=%s&tabchoice=revori&dtype=corr_for_efficiency&tresolution=%i%s&date_choice=bydate&start_year=%s&start_month=%s&start_day=%s&start_hour=%s&start_min=%s&end_year=%s&end_month=%s&end_day=%s&end_hour=%s&end_min=%s&yunits=0" \
            % (self.station, self.resolution_min, force_str,
            start.strftime('%Y'), start.strftime('%m'), start.strftime('%d'), start.strftime('%H'), start.strftime('%M'),
            end.strftime('%Y'), end.strftime('%m'), end.strftime('%d'), end.strftime('%H'), end.strftime('%M'))
        if self.verbose: print('i Request URL: ' + url)
            
        print("\n|  Downloading... ", end='')
        r = requests.get(url)
        if self.verbose: print('i received %s chars... ' % len(r.text), end='')
        # if date has not been covered we raise an error
        if str(r.text)[4:9]=='Sorry':
            raise ValueError('Request date is not avalaible at ',self.station, ' station, try other Neutron Monitoring station')
        # Write into file
        re_data_line = re.compile(r'^\d')
        with open(file, 'w') as f:
            for line in r.text.splitlines():
                if re_data_line.search(line):
                    f.write(line + "\n")
        file_save_msg(file, '')

        
def file_save_msg(file, name='File', end=''):
    size = os.stat(file).st_size
    if   size < 1024:    sizestr = "%.0f Bytes" % (size)
    elif size < 1024**2: sizestr = "%.0f KB"    % (size/1024)
    else:                sizestr = "%.1f MB"    % (size/1024/1024)
    print(cprintf("< %s saved to %s (%s)" % (name, file, sizestr), 'green'), end=end)
    

def ifdef(a, b, convert=float):
    if not a:
        return(b)
    else:
        return(convert(a))

# Split a string by , into a fixed length array
def xsplit(s, range=0, type=str):
    s = s.replace(',',' ')
    a = np.array(s.split(), dtype=type)
    # fill up with NaNs
    for i in np.arange(range-len(a)):
        a = np.append(a, np.nan)
    return(a)

# Split a string by \s,\s into an array
def csplit(s, range=0, type=str):
    s = re.sub(r'\s*,+\s*', ',', s)
    s = re.sub(r',+', ',', s)
    a = np.array(s.split(','), dtype=type)
    return(a)


re_crnsdata = re.compile(r'^\d+')
re_dataselect = re.compile(r'//DataSelect\s*=\s*(\w+)')
re_columns = re.compile(r'/+(RecordNum.+)')
re_columns2 = re.compile(r'/+(GpsUTC.+)')

def read(filename, archive=None, tz='UTC'):
    #print(archive)
#    if not archive is None:
#        archive = zipfile.ZipFile(archive_file, 'r')

    header_skip = 0
#    data_columns = ''
    datastr = ''
#    debug = 0
    with archive.open(filename,'r') if archive else open(filename, encoding="cp850") as file:
        for line in file:
#            debug += 1
#            if debug >= 1:
#                print(line)
            if isinstance(line, bytes):
                line = line.decode(errors='ignore')
            if re_crnsdata.search(line):
                datastr += line
#            else:
#                header_skip += 1
#                match_dselect = re_dataselect.search(line)
#                if match_dselect:
#                    data_select = match_dselect.group(1)
#                    continue
#                match_columns = re_columns.search(line)
#                if match_columns:
#                    data_columns = match_columns.group(1)
#                    continue
#                match_columns2 = re_columns2.search(line)
#                if match_columns2:
#                    data_columns += ',' + match_columns2.group(1)
#                    continue
                    
    #print('Parsed', header_skip, 'lines of auxiliary information.')
#    if not data_columns:
#        print('(No header in file %s)' % filename, end='')
#        return(None)
    
    #data_columns = data_columns.replace(',,',',')
#    data_columns = re.sub(r'\s*,+\s*', ',', data_columns)
#    data_columns = data_columns.split(',')
                
    # Read that file
#    filename_orig = filename
#    if archive:
#        filename = archive.open(filename,'r')
#    try:
#        data = pd.read_csv(filename, sep=',', skiprows=header_skip, names=data_columns,
#                    index_col=1, parse_dates=True, infer_datetime_format=True, encoding='cp850',
#                    error_bad_lines=False, warn_bad_lines=False)
#    except:
#        print("(Bad lines in file %s)" % filename_orig, end='')
#        return(None)
                    
    # Tidy up
#    data = data.loc[data.index.dropna()]
    
    ## Find out which column names are null
#    bad_columns = []
#    for c in range(len(data_columns)):
#        if pd.isnull(data_columns[c]):
#            bad_columns.append(c)
    ## Reassign data using only non-bad columns
#    data = data.iloc[:, [i for i,n in enumerate(data.columns) if i not in bad_columns]]
    
    return(datastr+"\n")


def chomp(x):
    if x.endswith("\r\n"): return x[:-2]
    if x.endswith("\n") or x.endswith("\r"): return x[:-1]
    return(x)
    
def read_header(filename, archive=None):

    import re
    import os
    re_crnsdata = re.compile(r'^\d+\,')
    re_strangecoord = re.compile(r'\d,[EW],')
    re_columns = re.compile(r'/+(RecordNum.+)')
    re_columns2 = re.compile(r'/+(GpsUTC.+)')
    strange_coords = []
    data_columns = ''
    col_row_detected=False
    with archive.open(filename,'r') if archive else open(filename) as file:
        for line in file:
            if isinstance(line, bytes):
                line = line.decode(errors='ignore')
                
            if re_crnsdata.search(line):
                # get column number of strange direction
                strange_coords = [line.count(',', 0, m.start())+1 for m in re_strangecoord.finditer(line)]
                if strange_coords:
                    #print(line)
                    break
            else:
                match_columns = re_columns.search(line)
                if match_columns:
                    data_columns = match_columns.group(1)
                    col_row_detected=True
                    continue
                match_columns2 = re_columns2.search(line)
                if match_columns2:
                    data_columns += ',' + match_columns2.group(1)
                    continue

    if len(data_columns)<=0:
        print('! Input file contains no headers (e.g., //RecordNum...). Try to define input_columns in the config file.')
        return(None)
    else:
        data_columns = chomp(data_columns)
        data_columns = csplit(data_columns)
        #re.sub(r'\s*,+\s*', ',', data_columns)
        #data_columns = re.sub(r',+', ',', data_columns)
        #data_columns = data_columns.split(',')
        if strange_coords:
            print("\n!  "+'Found strange coordinate format and missing columns, trying to fix it...', end='') 
            new_columns = ['NS','EW'] # Assuming that northing is first, easting is second.
            for pos in strange_coords:
                data_columns.insert(pos, new_columns.pop(0))
        #if the first row of col names was missing we manually add
        if not col_row_detected:
            print('Manually add half of column names')
            data_column_row=np.array(('RecordNum','Date Time(UTC)',
                                      'PTB110_mb','P4_mb','P1_mb','Vbat',
                                      'T1_C','RH1','N1Cts','N2Cts','N1ETsec',
                                      'N2ETsec','N1T(C)','N1RH','N2T(C)',
                                      'N2RH','T_CS215','RH_CS215'))
            #append together
            data_columns=np.append(data_column_row,data_columns[1:])
        return(data_columns)


       


def make_pandas(datastr, columns=None, header_skip=0, index=1):
    
    try:
        data = pd.read_csv(io.StringIO(datastr), encoding='cp850', skipinitialspace=True, 
                            sep=',',  skiprows=header_skip, names=columns,
                            error_bad_lines=False, warn_bad_lines=False, 
        # Depreciated: no longer convert columns directly on input:
        # parse_dates=True, infer_datetime_format=True, index_col=index, dtype = dtypes
        # dtypes = { c : float for c in columns }; dtypes[columns[index]] = str
                            dtype=object)
    except Exception as e:
        print("! ERROR interpreting data format: "+str(e))
        return(None)
                    
    # Tidy up
    data = data.loc[data.index.dropna()]
    len1 = len(data)
    
    ## Convert time column to a valid DateTime object and drop any failures
    data[columns[index]] = pd.to_datetime(data[columns[index]], errors='coerce')
    data = data.dropna(subset=[columns[index]])
    ## Set the DateTime column as index
    data.set_index(columns[index], inplace=True)
    ## Convert all the regular columns to numeric and drop any failures
    data = data.apply(pd.to_numeric, errors='coerce')
    
    ## Sort and unify the index and set the time zone as UTC
    data = data.sort_index().drop_duplicates().tz_localize('UTC')
    
    #print(data.index.dtype)
    
    len2 = len(data)
    if len2 < len1: print("i   Dropped %i malformed lines." % (len1-len2))

    ## Find out which column names are null
    bad_columns = []
    for c in range(len(columns)):
        if pd.isnull(columns[c]):
            bad_columns.append(c)
    ## Reassign data using only non-bad columns
    data = data.iloc[:, [i for i,n in enumerate(data.columns) if i not in bad_columns]]

    return(data)
    
    
# Save data as KML
def save_KML(data, var, lat, lon, file='test', format_str="%.0f%%", collim=None, cmap='Spectral_r'):
    
    data.loc[data[lat]==0.0, lat] = np.nan
    data.loc[data[lon]==0.0, lon] = np.nan
    data = data.dropna(subset=[var, lon, lat])

    if collim is None:
        xmin = np.min(data.loc[:,var])
        xmax = np.max(data.loc[:,var])
    else:
        xmin = float(collim[0])
        xmax = float(collim[1])
    #debug()
    data['color'] = None
    #data.loc[:,'color'] = None
    cmap = matplotlib.cm.get_cmap(cmap)
    if xmin < xmax:
        for i, row in data.iterrows():
            tmp = (data.at[i,var]-xmin)/(xmax-xmin)
            data.at[i,'color'] = matplotlib.colors.rgb2hex(cmap(tmp)[:3])
    
    kml = simplekml.Kml()
    
    for i, row in data.iterrows():
        label = format_str % row[var] if format_str else ''
        pnt = kml.newpoint(name=label, coords=[(row[lon],row[lat])])
        if row['color'] is not None: 
            pnt.style.iconstyle.color = row['color'].replace('#','#ff')
        pnt.style.iconstyle.scale = 1
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/pal2/icon18.png'
        pnt.style.labelstyle.scale = 0.8

    kml.save(file + '.kml')


# Figures

class Fig:
    fig = None # fig object given by pdfpages
    ax = None  # current single axis
    time_format = '%Y'
    layout = (1,1)
    axi = 1
    submode = False

    def __init__(self, fig=None, title='', layout=(1,1), xlabel='', ylabel='', size=(11.69,8.27), ylim=None, time_series=True):

        # Single PDF page when fig is provided
        if not fig is None:
            Fig.fig = fig
            plt.figure(figsize=size)
            Fig.layout = layout
            Fig.axi = 0
        
        if layout != (1,1):
            # For complex layout, do not do anything, just wait for next "with Fig()"
            Fig.submode = True
        else:
            Fig.axi += 1
            Fig.ax = plt.subplot(Fig.layout[0], Fig.layout[1], Fig.axi) 
            
            plt.title(title)
            Fig.ax.set(xlabel=xlabel, ylabel=ylabel)
            if ylim: Fig.ax.set_ylim(ylim)
            for a in ("top", "right"):
                Fig.ax.spines[a].set_visible(False)
                Fig.ax.get_xaxis().tick_bottom()
                Fig.ax.get_yaxis().tick_left()
            Fig.ax.grid(b=True, alpha=0.2)
            time_format = re.sub(r'(\w)', r'%\1', Fig.time_format)
            if time_series:
                Fig.ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter(time_format.replace('\\','')))
                plt.tick_params(labelsize=9)
    
    # Entering `with` statement
    def __enter__(self):
        return(Fig.ax) # makes possibe: with Fig() as ax: ax.change
    
    # Exiting `with` statement    
    def __exit__(self, type, value, traceback):
        # deactivate submode if axis counter exceeds layout shape
        if Fig.submode:
            if Fig.axi == Fig.layout[0]*Fig.layout[1]:
                Fig.submode = False
        else:
            # save and close PDF page
            Fig.fig.savefig(bbox_inches='tight')
            plt.close()
            
def yesno2bool(s):
    s = s.lower()
    if s == 'yes' or s == 'y':
        return(True)
    else:
        return(False)


def retrieve_dwd_data(data_subset,
                                  lon='LongDec',
                                  lat='LatDec',
                                  dwd_category='pressure',
                                  dwd_parameters=['air_pressure_local'],
                                  no_of_nearest_stations=4):    
    """
    A function to call the roverweb weather module to get dwd paramaters for 
    the desired location and time
    Warning: EPSG: 4326 is silently assumed
    Parameters
    ----------
    data_subset : dataframe
        DESCRIPTION.
    lon : str, optional
        data_subset Column name for longitude coordinate. The default is 'LongDec'.
    lat : str, optional
        data_subset Column name for latitude coordinate . The default is 'LatDec'.
    dwd_category : str, optional
        Parameter Category on FTP Server. The default is 'pressure'.
    dwd_parameters : list, optional
        Requested parameters for each category. The default is ['air_pressure_local'].
    no_of_nearest_stations : int, optional
        Number of nearest dwd stations for each record location. The default is 4.

    Returns
    -------
    data_subset : dataframe
        Output dataframe with new column

    """            
                                                 

    #remove rows with no geographic coordinate
    data_subset = data_subset[data_subset[lon].notna()]
    data_subset = data_subset[data_subset[lon].notna()]
    #create geodataframe
    data_subset=gpd.GeoDataFrame(data_subset,
                                 geometry=gpd.points_from_xy(data_subset[lon],
                                                             data_subset[lat]))
    print('download ',dwd_category, 'data on hourly base from nearest', 
          str(no_of_nearest_stations), 
          ' stations')
    data_subset,dwd_base=weather.Find_nearest_dwd_stations(
        data_subset.copy(),
        date_start=data_subset.index.min().date().strftime('%Y%m%d'),
        date_end=data_subset.index.max().date().strftime('%Y%m%d'),
        data_category=dwd_category,
        temp_resolution='hourly',
        no_of_nearest_stations=no_of_nearest_stations,
        memory_save=False,
        Output=True)
    # add data to rover record
    print('calculate dwd parameter value by idw method')
    data_subset=weather.Apnd_dwd_data(data_subset.reset_index(),
                                  dwd_base,
                                  time_col='Date Time(UTC)',
                                  data_time_format='%Y-%m-%d %H:%M:%S',
                                  data_category=dwd_category,
                                  parameters=dwd_parameters,
                                  no_of_nearest_stations=no_of_nearest_stations,
                                  idw_exponent=2
                                  )
    #restore index
    data_subset=data_subset.set_index('Date Time(UTC)')
    #create temporary columns from dwd data extraction
    dwd_temporary_columns=[dwd_category+'_station_'+str(i) for i in range(0,no_of_nearest_stations)]
    dwd_temporary_columns.extend([dwd_category+'_distance_'+str(i) for i in range(0,no_of_nearest_stations)])
    #drop them
    data_subset.drop(columns=dwd_temporary_columns,inplace=True)
    #remove geometry
    data_subset.drop(columns='geometry',inplace=True)
    print('Retrieved dwd data to correct', str(len(data_subset)), 'data records')
    return pd.DataFrame(data_subset)


def retrieve_soilgrids_data(data, lon='LongDec',lat='LatDec',                                         
                                soilgridlrs=['bdod','clay','soc'],
                                soil_layerdepths=['0-5cm','5-15cm','15-30cm','30-60cm'],
                                raster_res=(250, 250),
                                statistical_metric=['mean'],
                                all_touched=True,
                                zero_to_nan=True):
    """
    A function to call the roverweb soilgrids module to get SoilGrids paramaters for 
    the desired location and time
    Warning: EPSG: 4326 is silently assumed
    Parameters
    ----------
    data : dataframe
        DESCRIPTION.
    lon : str, optional
        data_subset Column name for longitude coordinate. The default is 'LongDec'.
    lat : str, optional
        data_subset Column name for latitude coordinate . The default is 'LatDec'.
    soilgridlrs : list, optional
        Parameter Category on WCS Service
    soil_layerdepths : list, optional
        Requested depth layers.
    all_touched : boolean, optional
    statistical_metric: list, optional
        Default is mean
    zero_to_nan :boolean, optional
        replace 0 in dataset by np.nan

    Returns
    -------
    data : dataframe
        Output dataframe with new column

    """
    #define a dictionary with names and conversion factors
    soil_metadata={'bdod':{'crr_name' : 'bd','conversion_factor':100},
                 'clay':{'crr_name' : 'clay','conversion_factor':1000},
                 'soc':{'crr_name' : 'org','conversion_factor':1000}}
    
    #remove rows with no geographic coordinate
    data = data[data[lon].notna()]
    data = data[data[lon].notna()]
    #create geodataframe
    data=gpd.GeoDataFrame(data,geometry=gpd.points_from_xy(data[lon],data[lat]),crs='epsg:4326')
    #remember all old columns
    col_nm_old=data.columns.values.tolist()
    #call the main functionality in roverweb
    data = soilgrid.apnd_from_wcs(data,soilgridlrs=soilgridlrs,
        soil_layerdepths=soil_layerdepths,
        raster_res=raster_res,
        statistical_metric=statistical_metric,
        all_touched=all_touched,
        output=None)
    #get the subset all all retrieved layers
    new_columns=list(set(data.columns.values.tolist()) - set(col_nm_old))
    data_subset=data[new_columns].copy()
    #replace 0 values with nan
    if zero_to_nan:
        print('replace zero entry by nan')
        data_subset.replace({0:np.nan},inplace=True)
    #rename and average columns
    if len(soil_layerdepths)>0:
        print('soil depth layers will be averaged')
        for soil_prop in soil_metadata:
            soil_prop_cols=[col for col in new_columns if soil_prop +'_' in col]
            #get mean of these columns as new column and divide by conversion factor
            data_subset[soil_metadata[soil_prop]['crr_name']] = data_subset[soil_prop_cols].mean(axis=1)/soil_metadata[soil_prop]['conversion_factor']
    
    #postprocessing, remove datasets and add
    #drop temporary datasets
    data_subset.drop(columns=new_columns,inplace=True)
    #add to original dataset
    data=data.join(data_subset)
    #remove geometry
    data.drop(columns='geometry',inplace=True)        
        
        
    
    print('Retrieval and processing from soilgrids succesful')                        

    return pd.DataFrame(data)

def retrieve_osm_overpass_data(data, lon='LongDec',lat='LatDec',crcl_radius=10,
                               no_of_clusters=None,
                               queryfeatures={'way': ['highway']},
                               output_col_name='road'):
    """
    A function to call the roverweb overpass osm module to get osm parameters for 
    the desired location and time. For more details check OVERPASS API docs
    Warning: EPSG: 4326 is silently assumed   

    Parameters
    ----------
    data : dataframe
        DESCRIPTION.
    lon : str, optional
        data_subset Column name for longitude coordinate. The default is 'LongDec'.
    lat : str, optional
        data_subset Column name for latitude coordinate . The default is 'LatDec'.
    crcl_radius : int, optional
        The radius of the circles created aroung each measurement point. The default is 10.
    no_of_clusters : int, optional
        The amount of cluster to be generated prior to calling Overpass API.
        The default is None, which means that the number of clusters is auto-generated
        from number of datapoints
    queryfeatures : dictionary, optional
        Features which are to retrieve from Overpass API. The default is {'way': ['highway']}.

    Returns
    -------
    data : TYPE
        Output dataframe with new column with new column entries

    """
    #remove rows with no geographic coordinate
    data = data[data[lon].notna()]
    data = data[data[lon].notna()]
    #create geodataframe
    data=gpd.GeoDataFrame(data,geometry=gpd.points_from_xy(data[lon],data[lat]),crs='epsg:4326')
    #create circles
    data_crlcs = geometry.points_to_circle(data, crcl_radius=crcl_radius, 
                                              number_of_points=10)
    #create number of clusters
    if no_of_clusters is None:
        print('automatic cluster generation')
        clusters=int(np.floor(np.log(len(data_crlcs))))
    else:
        clusters=int(no_of_clusters)
    #check whether at least one cluster will be generated
    if clusters<1:
        clusters=1
    #create the clusters
    data_clust = geometry.clustering(
        data_crlcs, clusters=clusters, cluster_fld_name='ClusterID')
    # group geodataframe
    data_grouped = data_clust.groupby('ClusterID')
    # create an empty result geodataframe
    output_data = gpd.GeoDataFrame()
    # inititate the loop
    for cluster_no in range(0, clusters):
        # create a subset of the original geodataframe
        data_subset = data_grouped.get_group(cluster_no)
        # create the queryboundstr
        querygeobound = osm.querybound_generation(data_subset)
        #call overpass API
        data_subset = osm.apnd_from_overpass(data_subset,querygeobound,
                                             queryfeatures=queryfeatures)
    
        # append subset back to entire dataset
        output_data = output_data.append(data_subset,sort=True)
        
    #remove geometry    
    output_data.drop(columns='geometry',inplace=True)
    #rename column from osm output
    output_data=output_data.rename(columns={output_data.iloc[:,-1].name:output_col_name})
    #sort it again, after mixing by cluster algorithm
    output_data=output_data.sort_index(ascending=True)
    return pd.DataFrame(output_data)


##################
######OSMNX_NEW_API###########
    #########

                                               


def Calibrate_N0_Desilets(N, sm, bd=1, lw=0, owe=0, a0=0.0808, a1=0.372, a2=0.115):
    return(N/(a0 / (sm/bd + a2 + lw + owe) + a1))

def N2SM_Desilets(N, N0, bd=1, lw=0, owe=0, a0=0.0808, a1=0.372, a2=0.115):
    return((a0/(N/int(N0)-a1)-a2 -lw - owe) * bd)

config = None

def main(configfile=None):
    print("Welcome to instant-PASDy %s!" % VERSION)

    # Config
    config = configparser.ConfigParser()
    if configfile is None: configfile = 'config.cfg'
    if not os.path.isfile(configfile):
        print('! Config file required! Either specify as argument or provide config.cfg')
        return()
    else:
        print("i Working with %s" % configfile)
            
    temp = config.read(configfile)
    
    # clean config
    if os.path.isdir(config['input']['path']):
        # add trailing slash
        config['input']['path'] = os.path.join(config['input']['path'], '')
    # check whether output path exists
    if config['output']['out_path']:
        if not os.path.exists(config['output']['out_path']):
            os.makedirs(config['output']['out_path'])
    # add trailing slash
    config['output']['out_path'] = os.path.join(config['output']['out_path'], '') 
    
    if 'update' in config and config['update']['data_source'] in ['FTP','SD']:
        print('| Updating data from ' + config['update']['data_source'] + ' into ' + config['input']['path'])
        download(archive_file=config['input']['path'], source=config['update']['data_source'],
            server=config['update']['FTP_server'], user=config['update']['FTP_user'],
            pswd=config['update']['FTP_pswd'], ftp_prefix=config['update']['FTP_prefix'],
            ftp_suffix=config['update']['FTP_suffix'], sd_path=config['update']['SD_path'],
            sd_prefix=config['update']['SD_prefix'], sd_suffix=config['update']['SD_suffix'])
#        download(archive_file=config['input']['path'], source='FTP', #config['update']['data_source'],
#            server=config['update']['FTP_server'], user=config['update']['FTP_user'],
#            pswd=config['update']['FTP_pswd'], ftp_prefix=config['update']['FTP_prefix'],
#            ftp_suffix=config['update']['FTP_suffix'], sd_path=config['update']['SD_path'],
#            sd_prefix=config['update']['SD_prefix'], sd_suffix=config['update']['SD_suffix'])

    
    ####################################################
    ######  Input  #####################################
    ####################################################
  
    delimiter = ''
    if config['input']['path'].endswith('.zip'):
        delimiter = '/'
    pattern = config['input']['path']+config['input']['data_prefix']+'*'+config['input']['data_suffix']
    print('> Reading data from ' + pattern+' ')
    
    archive = None
    files = []
    
    if config['input']['path'].endswith('.zip'):
        archive = zipfile.ZipFile(config['input']['path'], 'r')
        files = [x for x in archive.namelist() if x.endswith(config['input']['data_suffix']) and x.startswith(config['input']['data_prefix'])]
    
    elif os.path.isdir(config['input']['path']):
        files = glob(pattern)
    else:
        if 'gui' in globals():
            gui.Popup("Input path is neither a folder nor a zip archive.", title='Read ERROR', non_blocking=True)
        else:
            print("! ERROR: Input path is neither a folder nor a zip archive.")
        return(None)
    
    if len(files) == 0:
        print(cprintf('! No matching files found.', 'red'))
        return()
        
    datastr = ''
    data_columns = []
    if 'input_columns' in config['input']:
        if config['input']['input_columns'] != '':
            data_columns = csplit(config['input']['input_columns'])
        
    i = 0
    last_progress = 0 # in %
    
    for filename in files:
#        print('.', end='')
        #print(filename)
        #progressbar(i-1, M, title='Reading', prefix=filename)
        i += 1
        progress = (i+0.01)/len(files)//0.1*10 # in %, offset 0.01 because x/x//0.1 is 9
        if progress > last_progress:
            print("%.0f%%.." % progress, end='')
            last_progress = progress
            
        if datastr=='':
            if data_columns == []:
                data_columns = read_header(filename, archive)
            datastr = read(filename, archive)
        else:
            datastr += read(filename, archive) #data.append() #, verify_integrity=True)
    # for old records, for some reasons, one 
    #progressbar(i-1, len(files), title='Reading', prefix=filename, end=True)                                                                                      
    if not archive is None: archive.close()
    #DEBUG output raw data file
    #with open('temp.txt','a') as f:
    #    f.write(datastr)
    print("\n"+'| Conversion to data frame...')
    data = make_pandas(datastr, data_columns)
    if data is None:
        print('! Error: Cannot interprete the data. Check the column header, is this correct? '+ ','.join(data_columns))
        return()
    if len(data)==0:
        raise ValueError('No record in dataset')
        
    ####################################################
    ######  Basic cleaning  ############################
    ####################################################
    
    if 'bbox' in config['clean'] and config['clean']['bbox']:
        bbox = xsplit(config['clean']['bbox'], type=float)
    else:
        bbox = []
    
    N = config['correction']['new_neutron_column']
    tres = config['input']['resolution_column']
    lat = config['input']['latitude_column']
    lon = config['input']['longitude_column']
    alt = config['input']['altitude_column']
    p  = config['input']['pressure_column']
    rh = config['input']['humidity_column']
    T  = config['input']['temperature_column']
    ah = config['correction']['new_humidity_column']
    inc = config['correction']['new_incoming_column']
    # Thermal neutrons
    th = '_thermal'
    # Cleaned data
    clean = '_clean'
    # Corrected data
    pih = '_pih'
    # Suffix for vairable errors
    err = '_err' # followed by _sym, _low, _upp
    # Standard deviation
    std = '_std'
    # Smooth
    mov = '_mov'
    # Biomass
    bio = '_bio'
    # Urban
    urb = '_urb'
    # Road
    road = '_road'
        
    # check wrong column names
    for name in [p, rh, T]:
        if not name in data.columns:
            print('! Warning: %s is not a column name.' % name)
            data[name] = np.nan
    
    print('i   Data period from', data.index.min().strftime('%Y-%m-%d %H:%M:%S'),
                            'to', data.index.max().strftime('%Y-%m-%d %H:%M:%S'))

    # Set time resolution
    if not tres or not tres in data.columns:
        tres = 'N1ET_sec'
        if 'resolution_default' in config['input'] and config['input']['resolution_default']:
            data[tres] = int(config['input']['resolution_default'])
        else:
            # infer from timestamp difference
            try:
                data[tres] = data.index.to_series().diff() #.median().seconds
            except:
                # set 60 sec as the fallback time resolution
                data[tres] = 60 
    
    print('i Time resolution approx. %.0f sec.' % data[tres].median())
    
    ## Sum up neutron columns 
    data[N] = 0
    for c in xsplit(config['input']['neutron_columns']):
        data[N] += data[c]
    
    data[N+err] = np.sqrt(data[N])
    report_N(data, N, units='raw counts')
        
    # convert to units of cph
    data[N]     = data[N] / data[tres] * 3600
    data[N+err] = data[N+err] / data[tres] * 3600
    
    report_N(data, N)
    
    # Thermal neutrons
    if config['input']['thermal_neutron_columns']:
        
        # sum up thermal neutrons
        data[N+th] = 0
        for c in xsplit(config['input']['thermal_neutron_columns']):
            data[N+th] += data[c]
        
        data[N+th+err] = np.sqrt(data[N+th])
        
        # convert to units of cph
        data[N+th]     = data[N+th] / data[tres] * 3600
        data[N+th+err] = data[N+th+err] / data[tres] * 3600
   
        Nth = N+th
    else:
        Nth = None
    
    
    ####################################################
    ######  Clean/filter  ##############################
    ####################################################
        
    print('| Cleaning... ', end='')
    
    #first we clean non valid geometries

    #JJ Strangely formatted coordinates occur for some Rovers (Jülich, Melbourne)
    
    if 'EW' in data.columns or 'NS' in data.columns:
        # Remove cardinal direction if it is attached to the coordinate (has been necessary in some configurations)
        data[lat] = pd.to_numeric(data[lat].astype(str).replace({'N':''}, regex=True), errors='coerce') # replace N if existent in column and convert to numeric
        data[lon] = pd.to_numeric(data[lon].astype(str).replace({'E':''}, regex=True), errors='coerce') # replace E if existent in column and convert to numeric
        # Convert to readable coordinate
        data[lat] = deg100min2dec( data[[lat, 'NS']] )
        data[lon] = deg100min2dec( data[[lon, 'EW']] )

    # Clean Latlon
    if lat != '' and lat in data.columns:    
        print('| Cleaning coordinates...')
        if lat in data.columns: data.loc[data[lat] == 0.0, lat] = np.nan
        if lon in data.columns: data.loc[data[lon] == 0.0, lon] = np.nan
        data = data.dropna(subset=[lat])
        data = data.dropna(subset=[lon])
        data['lon'] = data[lon]
        data['lat'] = data[lat]

        if len(bbox)>0:
            data = data.query("lat > %s and lat < %s" % (bbox[2], bbox[3]))
            data = data.query("lon > %s and lon < %s" % (bbox[0], bbox[1]))
            if len(data) == 0:
                print(cprintf('! You seem to have erased the whole dataset using an unfortunate bbox.', 'red'))
                return()
        try:
            data['ll'] = data.apply(lambda row: tuple((row['lat'],row['lon'])), axis=1)
        except ValueError:
            raise ValueError('NO GPS Data Available, processing stopped')
        #D = D.dropna(subset=['lat'])
        # Lat lon to UTM
        utmy, utmx = latlon2utm(data.lat, data.lon)
        data['utmy'] = utmy
        data['utmx'] = utmx
        

    # if no geographic information available we stop processing here
    if data[lat].isna().sum()==len(data) or data[lon].isna().sum()==len(data):
        raise ValueError('NO GPS Data Available, processing stopped')
    
    ##replace Altitude colum with data from DEM#
    if config['clean']['altitude_raster']:
        data = get_from_raster(data, alt, config['clean']['altitude_raster'])
        print('Altitude data replaced by values from external DEM source')
    
    
    
    # Get upper and lower bounds from config
    (Na,Nb)  = xsplit(config['clean']['neutron_range'], range=2, type=float)
    (pa,pb)  = xsplit(config['clean']['pressure_range'], range=2, type=float)
    (rha,rhb)= xsplit(config['clean']['humidity_range'], range=2, type=float)
    (Ta,Tb)  = xsplit(config['clean']['temperature_range'], range=2, type=float)
    if 'timeres_range' in config['clean']:
        (tra,trb)= xsplit(config['clean']['timeres_range'], range=2, type=float)
    
    # Define new column N+clean
    data[N+clean] = data[N]
    data[N+clean+err] = data[N+err]
    Ncleanstr = N+clean
    
    if Nth:
        data[N+th+clean] = data[N+th]
        data[N+th+clean+err] = data[N+th+err]
    
    # Drop lines option
    if config['clean']['invalid_data'] == 'drop lines':
        len_before = len(data)
        data = data.query("@Ncleanstr > %s and @Ncleanstr < %s" % (Na, Nb))
        data = data.query("@p > %s and @p < %s" % (pa, pb))
        data = data.query("@rh> %s and @rh< %s" % (rha,rhb))
        data = data.query("@T > %s and @T < %s" % (Ta, Tb))
        if 'timeres_range' in config['clean']:
            data = data.query("@tres > %s and @tres < %s" % (tra, trb))
        print("dropped %s invalid lines." % (len_before - len(data)))
    # Fill´-na option
    else:
        nan_before = data.isnull().sum().sum()
        data.loc[(data[Ncleanstr] < Na ) | (data[Ncleanstr] > Nb) , Ncleanstr ] = np.nan
        data.loc[(data[Ncleanstr] < Na ) | (data[Ncleanstr] > Nb) , Ncleanstr+err ] = np.nan
        if config['clean']['timeres_range']:
            print('clean data out of defined timeresolution range')
            data.loc[(data[tres] < tra ) | (data[tres] > trb) , Ncleanstr ] = np.nan

            
            
        # now we decide how we treat the meteorological values out of range
        
        #start with is air moisture
        if config['clean']['missing_humidity'] =='DWD' and len( data.loc[(data[rh]< rha) | (data[rh]> rhb), rh])>0:
            #get subset with values out of range
            data_subset=data.loc[(data[rh]< rha) | (data[rh]> rhb)]
            #call API Functionality to DWD
            print('| Looking up air humidity from external source...', end='')
            data_subset=retrieve_dwd_data(data_subset,
                      lon=lon,lat=lat,dwd_category='air_temperature',
                      dwd_parameters=['2m_relative_humidity'],
                      no_of_nearest_stations=4)
            #finally replace the values in the dataframe
            data.loc[data_subset.index,rh] = data_subset['2m_relative_humidity']
            print(' %.1f +/- %.1f g/m3' % (data_subset['2m_relative_humidity'].mean(), data_subset['2m_relative_humidity'].std()))
        elif len( data.loc[(data[rh]< rha) | (data[rh]> rhb), rh])>0:
            data.loc[(data[rh]< rha) | (data[rh]> rhb), rh] = ifdef(config['clean']['missing_humidity'], np.nan)
            
        #the temperature
        if config['clean']['missing_temperature'] =='DWD' and len( data.loc[(data[T] < Ta ) | (data[T] > Tb) , T ] )>0:
            #get subset with values out of range
            data_subset=data.loc[(data[T] < Ta ) | (data[T] > Tb)] 
            #call API Functionality to DWD
            print('| Looking up air temperature from external source...', end='')
            data_subset=retrieve_dwd_data(data_subset,
                      lon=lon,lat=lat,dwd_category='air_temperature',
                      dwd_parameters=['2m_air_temperature'],
                      no_of_nearest_stations=4)
            #finally replace the values in the dataframe
            data.loc[data_subset.index,T] = data_subset['2m_air_temperature']
            print(' %.1f +/- %.1f C' % (data_subset['2m_air_temperature'].mean(), data_subset['2m_air_temperature'].std()))
        elif len( data.loc[(data[T] < Ta ) | (data[T] > Tb) , T ] )>0:   
            data.loc[(data[T] < Ta ) | (data[T] > Tb) , T ] = ifdef(config['clean']['missing_temperature'], np.nan)
        
        #finally the pressure
        
        if config['clean']['missing_pressure'] =='DWD' and len(data.loc[(data[p] < pa ) | (data[p] > pb) , p ])>0:
            print('correct missing values using dwd database')
            #get subset with values out of range
            data_subset=data.loc[(data[p] < pa ) | (data[p] > pb)]
            #call API Functionality to DWD
            print('| Looking up air pressure from external source...', end='')                                                                 
            data_subset=retrieve_dwd_data(data_subset,
                      lon=lon,lat=lat,dwd_category='pressure',
                      dwd_parameters=['air_pressure_nn'],
                      no_of_nearest_stations=10)
            # we use the barometric formular to correct pressure data
            #p=p0*(1-0.0065h/(T+0.0065h+273.15))^5.257
            data_subset['air_pressure_local']=data_subset['air_pressure_nn']*(1-(0.0065*data_subset[alt])/(data_subset[T]+0.0065*data_subset[alt]+273.15))**5.257
            #finally replace the values in the dataframe
            data.loc[data_subset.index,p] = data_subset['air_pressure_local']
            #remove 0 values, as they come from defect pressure measurements
            data=data[data[p]!=0]
            data_subset=data_subset[data_subset[p]!=0]
            print(str(sum(data[p]==0)), 'of ', str(len(data)),'records removed due invalid dwd pressure data')
            print(' %.1f +/- %.1f hPa' % (data_subset['air_pressure_local'].mean(), data_subset['air_pressure_local'].std()))
            if len(data)==0:
                raise ValueError('No entries with valid pressure from dwd left')
        elif len(data.loc[(data[p] < pa ) | (data[p] > pb) , p ])>0:
            #if replacement value given, we use it, if nothing given, we use np.nan
            data.loc[(data[p] < pa ) | (data[p] > pb) , p ] = ifdef(config['clean']['missing_pressure'], np.nan)        
        
        
        #result                                                               
        print("replaced %s values by NaN." % (data.isnull().sum().sum() - nan_before))
      
        
    # Interpolate over nans
    if yesno2bool(config['clean']['interpolate_neutrons']):
        data[N+clean]     = data[N+clean].interpolate()
        data[N+clean+err] = data[N+clean+err].interpolate()
        if Nth:
            data[N+th+clean]     = data[N+th+clean].interpolate()
            data[N+th+clean+err] = data[N+th+clean+err].interpolate()
    
    report_N(data, N+clean)
    
    ####################################################
    ######  Soil #######################################
    ####################################################
    
    print('> Sampling land data... ', end='')
    #depending on Option for soil data retrieval we add soil data
    if int(config['conversion']['get_soil_data_option'])==0:
        print('Retrieve soil properties from constant values')
        #add static data
        data['bd']  = float(config['conversion']['bulk_density'])
        data['org'] = float(config['conversion']['soil_org_carbon'])
        data['lw']  = float(config['conversion']['lattice_water'])
    # add from raster datasets
    if int(config['conversion']['get_soil_data_option'])==1:
        print('Retrieve soil properties from static maps')
        if lat in data.columns:        
            if config['conversion']['bulk_density_raster']:
                data = get_from_raster(data, 'bd',   config['conversion']['bulk_density_raster'])
            if config['conversion']['clay_content_raster']:
                data = get_from_raster(data, 'clay', config['conversion']['clay_content_raster'])
                
            if config['conversion']['soil_org_carbon_raster']:
                data = get_from_raster(data, 'org',  config['conversion']['soil_org_carbon_raster'])
    
    # add from soilgrids
    if int(config['conversion']['get_soil_data_option'])==2: 
        print('Retrieve soil properties from SoilGrids API')
        data=retrieve_soilgrids_data(data.copy(),lon='LongDec',lat='LatDec',
                                        soilgridlrs=['bdod','clay','soc'],
                                        soil_layerdepths=['0-5cm','5-15cm','15-30cm','30-60cm'],
                                        raster_res=(250, 250),
                                        statistical_metric=['mean'],
                                        all_touched=True
                                        )        
    #convert organic carbon and clay
    data['owe'] = data['org']  *0.556
    data['lw']  = data['clay'] *0.1783
    
    #add landuse data
    if lat in data.columns: 
        if config['conversion']['land_use_raster']:
            data = get_from_raster(data, 'luse', config['conversion']['land_use_raster'])
            
    print('bulk dens. (%.2f g/cm3), lattice w. (%.2f g/g), org.we. (%.2f g/g)' % (data.bd.mean(), data.lw.mean(), data.owe.mean()))
    
        
    ####################################################
    ######  Incoming  ##################################
    ####################################################
    
    print('> Reading incoming radiation data from ' + config['correction']['NM_path'], end='')
    M = NM(config=config).get(start=data.index.min().replace(tzinfo=pytz.UTC), end=data.index.max().replace(tzinfo=pytz.UTC))
    #import pdb
    #pdb.set_trace()
    data.index = data.index.tz_localize(None)
    M.data.index = M.data.index.tz_localize(None)
    M.data = M.data.reindex(data.index, method='nearest', limit=1, tolerance=timedelta(seconds=M.resolution_min*60/2-1)).interpolate()
    M.data.index = M.data.index.tz_localize('UTC')
    data.index = data.index.tz_localize('UTC')
    
    ####################################################
    ######  pih  #######################################
    ####################################################
    print("\n| Corrections... ", end='')
    data[inc] = M.data
    #data[ah] = 6.112*np.exp(17.67*14/(243.5+14))/(273.15 + 14) * 2.1674 * 80
    data[ah] = 6.112*np.exp(17.67*data[T]/(243.5+data[T]))/(273.15 + data[T]) * 2.1674 * data[rh]
    if config['correction']['humidity_method'] == 'Rosolem et al. (2013)':
        data['correct_h'] = 1 + float(config['correction']['alpha']) * (data[ah] - float(config['correction']['humidity_ref']))
    else:
        data['correct_h'] = 1
    data['correct_p'] = np.exp((data[p] - float(config['correction']['pressure_ref'])) / float(config['correction']['beta']))
    data['correct_i'] = 1/(1 - float(config['correction']['gamma']) * (1 - data[inc] / float(config['correction']['incoming_ref']))) 
    
    data[N+clean+pih]     = data[N+clean]     * data['correct_p'] * data['correct_i'] * data['correct_h']
    data[N+clean+pih+err] = data[N+clean+err] * data['correct_p'] * data['correct_i'] * data['correct_h']
    
    if Nth:
        data[N+th+clean+pih]     = data[N+th+clean]     * data['correct_p'] * data['correct_i'] * data['correct_h']
        data[N+th+clean+pih+err] = data[N+th+clean+err] * data['correct_p'] * data['correct_i'] * data['correct_h']
    
    print("for pressure (%+.0f%%), abs. humidity (%+.0f%%), incoming radiation (%+.0f%%)" % \
        (100*(data['correct_p'].mean()-1), 100*(data['correct_h'].mean()-1), 100*(data['correct_i'].mean()-1)))

    Nfinal = N+clean+pih
    report_N(data, N+clean+pih, correction=['correct_p','correct_i','correct_h'])
    #If no data remain with valid corrected data, we stop processing
    if data[N+clean+pih].isna().sum()==len(data):
        raise ValueError('No record with valid meteo OR LAT/LON OR0 Neutron Range exists, stop further processing')
    
    
    ####################################################
    ######  POI  #######################################
    ####################################################
    
    if 'poi_json' in config['conversion'] and config['conversion']['poi_json']: # or config['conversion']['poi_lat']:
        print('| Evaluating points of interest...')
        poi = '_poi'
        POI = read_geojson(config['conversion']['poi_json']) # 'data/Selke.geojson'
        utmy, utmx = latlon2utm(POI.lat, POI.lon)
        POI['utmy'] = utmy
        POI['utmx'] = utmx
        iax = -1
        max_radius = config['conversion']['poi_radius']
        
        data[Nfinal+'_poi'] = data[Nfinal]
        data[Nfinal+'_poi_err'] = data[Nfinal+err]
        Nfinal += '_poi'

        for i, row in POI[POI.type=='UFZ'].iterrows():
            data['distance_poi'] = np.sqrt((data.utmx-row['utmx'])**2+(data.utmy-row['utmy'])**2)
            iax += 1
            data.loc[data.distance_poi<max_radius, Nfinal] = data.loc[data.distance_poi<max_radius, Nfinal].mean()   
            data.loc[data.distance_poi<max_radius, Nfinal+err] = data.loc[data.distance_poi<max_radius, Nfinal+err].mean() / np.sqrt(data.loc[data.distance_poi<max_radius, Nfinal+err].count())
            print('i   %20s %.0f' % (row['label'], data.loc[data.distance_poi<max_radius, Nfinal].mean()))
    else:
        poi = ''
    
    ####################################################
    ######  Urban  #####################################
    ####################################################
    
    data['correct_urban'] = 1
    
    if 'land_use_urban_corr' in config['conversion'] and config['conversion']['land_use_urban_corr'] != '':
        if config['conversion']['land_use_urban_corr'] == 'nan':
            # a factor of one will not result in any change
            data.loc[data.luse_str=='urban', 'correct_urban'] = 1
        else:
            data.loc[data.luse_str=='urban', 'correct_urban'] = float(config['conversion']['land_use_urban_corr'])
        
        data[Nfinal+urb] = data[Nfinal] * data['correct_urban']
        data[Nfinal+urb+err] = data[Nfinal+err] * data['correct_urban']
        Nfinal += urb
        report_N(data, Nfinal, 'correct_urban')
    else:
        urb = ''
    
    ####################################################
    ######  Bio  #######################################
    ####################################################
    ## vegetation correct
    ## assume neutron counts were reduced by 10% for every measurement in a forest 
    data['correct_bio'] = 1
    data['correct_bio_err'] = 0
    if 'land_use_forest_corr' in config['conversion'] and config['conversion']['land_use_forest_corr'] != '':
        if config['conversion']['land_use_forest_corr'] == 'nan':
            data.loc[data.luse_str=='forest', 'correct_bio'] = 1
        else:
            data.loc[data.luse_str=='forest', 'correct_bio'] = float(config['conversion']['land_use_forest_corr'])
        
        print("| Correction for biomass (%+.0f%%)." % (100*(data.loc[data.luse_str=='forest', 'correct_bio'].mean()-1)))
        # well do that after road correction
        #data[Nfinal+'_bio'] = data[Nfinal] * data['correct_bio']
        #data[Nfinal+'_bio'+err] = data[Nfinal+err] * data['correct_bio']
        #Nfinal += '_bio'
        #report_N(data, Nfinal, 'correct_bio')                         
    else:
        bio = ''
        #print('.')
    
    report_N(data, N+clean+pih)
    report_N(data, Nfinal, 'correct_urban')
    
    ####################################################
    ######  Road  ######################################
    ####################################################
    
    data['correct_road'] = 1
    if config['conversion']['road_method'] != '' and config['conversion']['road_method'] != 'None' and lat in data.columns:
        
        print('| Correction for road effect...')
        # Load road properties (goes to config?)
        
        #print('|   Loading road type correction properties...')
        print(cprintf('>   %s' % config['conversion']['road_type_table'], 'yellow'))
        Roads = pd.read_csv(config['conversion']['road_type_table'], sep=r'\s+', skipinitialspace=True, comment='#')
        
        ####Now we decide whether we get the road_network_Data####
        ####from osmnx or fromroverweb Overpass API####
        if config['conversion']['get_road_network_option']=='1':
            print('retrieve road network information from osmnx')
            # Load road network
            #print('>   ', end='') # Graph prints a message.
            road_network_file = config['conversion']['road_network_file']
            
            # Load nearest road types        
            road_type_file = configfile[0:configfile.index('.cfg')]+'.roads'
            
            # Derive bbox from data if not set in config
            if len(bbox)<=0:
                bbox = [data[lon].min(), data[lon].max(), data[lat].min(), data[lat].max()]
    
            print('|   Loading road network... ', end='')
            if os.path.exists(road_type_file):
                # Read from file
                #print('|   Loading nearest road types...')
                print(cprintf('\n>   %s' % road_type_file, 'yellow'))
                roadsdf = pd.read_csv(road_type_file, index_col=0, parse_dates=True, infer_datetime_format=True)
                data['road'] = roadsdf
            else:
                #if config['conversion']['road_network_source'] == 'osmnx':
                R = Graph('roads', bbox, file=road_network_file, report_save='<   Saved')
                R.get(save=False) # read from file, or download and save
            
                if len(R.data)<=0:
                    print('i   Road network is empty!')
                else:
                    # Calculate
                    print('|   Searching nearest roads...')
                    # invoke progress bar feature
                    from tqdm import tqdm
                    tqdm.pandas()
                    
                    data['road'] = data.progress_apply(lambda row: R.on_road_type(point=row['ll']), axis=1)
                    
                    #print('    Set priorities...')
                    for i, row in data.iterrows():
                        row = row.copy()
                        if isinstance(row.road, list):
                            if 'primary' in row.road:
                                data.loc[i, 'road'] = 'primary'
                            elif 'track' in row.road:
                                data.loc[i, 'road'] = 'track'
                        #print(D.loc[i, 'road'], end=' ')
                    #print('')
                    
                    # Save backup
                    data['road'].to_csv(road_type_file)
                    file_save_msg(road_type_file, 'Nearest roads', end="\n")
        elif config['conversion']['get_road_network_option']=='2':
            print('retrieve road network information from overpass API')
            data=retrieve_osm_overpass_data(data, lon='LongDec',lat='LatDec',crcl_radius=50,
                               no_of_clusters=None,queryfeatures={'way': ['highway']},
                               output_col_name='road')
        # Set road values
        data['road_moisture'] = np.nan
        data['road_width'] = 0.0
        if 'road' in data.columns:
            for i, row in Roads.iterrows():
                row = row.copy()
                data.loc[data.road==row['category'], 'road_moisture'] = row['moisture']
                data.loc[data.road==row['category'], 'road_width']    = row['width']
        else:
            print('|   Applying constant road parameters theta=%s, width=%s' % (config['conversion']['road_moisture'], config['conversion']['road_width']))
            data['road_moisture'] = float(config['conversion']['road_moisture'])
            data['road_width']    = float(config['conversion']['road_width'])
        #pdb.set_trace()
        # correct for biomass to avoid smearing out
        data[Nfinal+'_temp'] = data[Nfinal] * data['correct_bio']
        data = moving_avg(data, Nfinal+'_temp', window=config['clean']['smooth'], err=False, std=False)
        # back to Nfinal, but mavged
        data[Nfinal+'_temp_mov'] /= data['correct_bio'] 
        # N2SM first order
        if config['conversion']['N0']:
            N0 = config['conversion']['N0']
        else:
            N0 = data[Nfinal].max()*1.1
            print('! N0 is not defined, but we need it for road correction. Guessing %.0f cph.' % N0)
        data['sm_temp'] = N2SM_Desilets(data[Nfinal+'_temp_mov'], N0, bd=data.bd, lw=data.lw, owe=data.owe,
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
        # C_road
        data['correct_road'] = 1/(1 + 0.42*(1-np.exp(-0.5*data.road_width)) * (1.06-4*data.road_moisture-(0.16 + data.road_moisture)/(0.39 + data['sm_temp'])))
        data.loc[(data['correct_road'] > 1), 'correct_road'] = 1
        #data.loc[data.road_width==0, 'correct_road'] = 1
        data.correct_road = data.correct_road.fillna(1)
        print("i   ...road effect (%+.0f%%)." % (100*(data['correct_road'].mean()-1)))
        
        # tidy up temp columns
        #data.drop([Nfinal+'_temp', 'sm_temp'], axis=1, inplace=True)
                
    else:
        road = ''
    
        
    # Set N_bio
    data[Nfinal+bio]          = data[Nfinal]         * data['correct_bio']
    
    if 'land_use_forest_corr_err' in config['conversion']:
        data['correct_bio_err'] = float(config['conversion']['land_use_forest_corr_err'])
    data[Nfinal+bio+err] = errpropag_factor(data, Nfinal, 'correct_bio', err)
    #data[Nfinal+bio+err]      = data[Nfinal+err]     * data['correct_bio']
    
    Nfinal += bio
    report_N(data, Nfinal, 'correct_bio')
        
    if config['conversion']['road_method'] != '' and config['conversion']['road_method'] != 'None'  and lat in data.columns:
    
        # set N_road
        data[Nfinal+road]     = data[Nfinal]     * data['correct_road']
        
        data['correct_road_err'] = float(config['conversion']['road_correction_err'])
        data[Nfinal+road+err] = errpropag_factor(data, Nfinal, 'correct_road', err)
        #data[Nfinal+road+err] = data[Nfinal+err] / data['correct_road']
        
        Nfinal += road
        report_N(data, Nfinal, 'correct_road')
   
    #if config['conversion']['road_moisture'] and float(config['conversion']['road_width']) > 0 and not 'correct_road' in data.columns:
     #           print("| Corrections for road effect... ", end='')
                #if   config['correction']['road_material'] == 'stone':    road_theta = 0.04
                #elif config['correction']['road_material'] == 'concrete': road_theta = 0.07
                #elif config['correction']['road_material'] == 'asphalt':  road_theta = 0.10
      #          road_theta = float(config['conversion']['road_moisture'])
    
    # Aggregate
    if config['clean']['aggregate']:
        agg = config['clean']['aggregate']
        if agg.isdigit(): agg = str(agg)+'hour'
        print('| Aggregate data to '+agg+' time steps...', end='')
        data_orig = data.copy() 
        window = agg.replace('s','')
        window = window.replace('hour','H').replace('min','Min').replace('day','D').replace('week','W').replace('month','M').replace('year','A')
        
        tresbefore = data.index.to_series().dropna().diff().median().total_seconds()
        
        aggobj = data.resample(window)
        data = aggobj.mean()
        
        tresafter = data.index.to_series().dropna().diff().median().total_seconds()
        # factor by which the time resolution is scaled up 
        tresfactor = tresafter / tresbefore
        tresfactor_err = 1 / np.sqrt( tresfactor )
        # number of hours per new time resolution
        hours = tresafter / 3600
        
        print(' (factor %0.2f, error %.02f)' % (tresfactor, tresfactor_err))
        
        ## Errors cannot be aggregated like this! They should scale with sqrt(1/tresfactor)
        # Errors so far: Nfinal #N_err, N_clean_err, N_clean_pih_err
        for n in [Nfinal]: #, N+clean, N+clean+pih, N+clean+pih+urb, N+clean+pih+urb+bio, N+clean+pih+urb+bio+road]:
            # theortical standard deviation
            data[n+err] *= tresfactor_err
            # actual standard deviation
            data[n+std] = aggobj.std()[n]
        
        report_N(data, Nfinal)
        
        if Nth:
            for n in [N+th, N+th+clean, N+th+clean+pih]:
                # theortical standard deviation
                data[n+err] *= tresfactor_err
                # actual standard deviation
                data[n+std] = aggobj.std()[n]
    
    # Define the N which to convert to SM
    #Nfinal = N+clean+pih
    #Nthfinal = N+th+clean+pih
    
    ## Smooth
    if config['clean']['smooth']:
        print('| Smooth data by a moving average over '+config['clean']['smooth']+' time steps...')
        
        data = moving_avg(data, Nfinal, window=config['clean']['smooth'])
        Nfinal += mov
        report_N(data, Nfinal)

        #window = int()
        #rollobj = data[N+clean+pih].rolling(window, center=True)
        #data[N+clean+pih+mov]     = rollobj.mean()
        #data[N+clean+pih+mov+std] = rollobj.std()
        #data[N+clean+pih+mov+err] = data[N+clean+pih+err] * 1/np.sqrt(window)
        
        if Nth:
            data = moving_avg(data, Nthfinal, window=config['clean']['smooth'])
            Nthfinal += mov
            
            #rollobj = data[N+th+clean+pih].rolling(window, center=True)
            #data[N+th+clean+pih+mov]     = rollobj.mean()
            #data[N+th+clean+pih+mov+std] = rollobj.std()
            #data[N+th+clean+pih+mov+err] = data[N+th+clean+pih+err] * 1/np.sqrt(window)
    else:
        mov = ''

    
    # Calculate footprint length
    if 'Speed_kmh' in data.columns:
        tres = data.index.to_series().dropna().diff().median().total_seconds()
        data['footprint_length'] = data['Speed_kmh']/3.6 * tres
        print('i   Average footprint length is %.0f m.' % data.loc[data['footprint_length'] > 2.0, 'footprint_length'].mean())
        
        
    ##JJ smooth with nearest neighbors
    if config['clean']['number_locations']:
        print('| Smooth data with '+config['clean']['number_locations']+' nearest neighbors.')
        data = data.dropna(subset=['LatDec', 'LongDec']) # drop rows without Latitude or Longitude information
        maxdist, RecNums = Neighbours(data[lat], data[lon], int(config['clean']['number_locations']), data)
        
        data['maxdistance'] = maxdist
        data['neighbors_RecordNum'] = RecNums.tolist()
        
        test = []
        Recind = []
        res = []
        result = []
        resTh = []
        result_Th = []
        
        for i in range(len(data)): # store the nearest recordnumber in neighbors
            test = data['RecordNum'].iloc[data['neighbors_RecordNum'][i]].values
            Recind.append(test.tolist())
        
        data['neighbors_RecordNum'] = Recind
        
        for i in range(len(data)): # calculate average N from neighbors index
            res = data[Nfinal][data['RecordNum'].isin(data['neighbors_RecordNum'][i])].mean()
            result.append(res)
            if Nth:
                resTh = data[Nthfinal][data['RecordNum'].isin(data['neighbors_RecordNum'][i])].mean()
                result_Th.append(resTh)
        
        Nfinal += '_neighbors'
        Nthfinal += '_neighbors'
        data[Nfinal] = result
        data[Nthfinal] = result_Th
            
    
    ## Select a certain period
    if config['clean']['start'] or config['clean']['end']:
        if config['clean']['start']:
            start = dateutil.parser.parse(config['clean']['start']).replace(tzinfo=pytz.UTC)
        else:
            start = data.index.min()
        if config['clean']['end']:
            end = dateutil.parser.parse(config['clean']['end']).replace(tzinfo=pytz.UTC)
        else:
            end = data.index.max()
        data = data.loc[start:end]
    
    
    start_str = data.index.min().strftime('%Y%m%d%H')
    end_str = data.index.max().strftime('%Y%m%d%H')
    end_str = '-'+end_str if start_str != end_str else '' 
    output_basename = os.path.join(config['output']['out_path'],'') + config['output']['out_basename'] + '-' \
                      + start_str + end_str #config['input']['data_prefix']
    
    
    #debug()
    
    ####################################################
    ######  Soil Moisture  #############################
    ####################################################
    if config['conversion']['N2sm_method'] != 'None':     
        
        # Calibrate
        N0 = config['conversion']['N0']
        calibration = False
        if not N0:
            print('| Calibration... ', end='')
            calibration = True
            start = dateutil.parser.parse(config['conversion']['campaign_start'])
            end = dateutil.parser.parse(config['conversion']['campaign_end'])
            datac = data.copy()
            datac = datac.loc[start:end]
            Nc = datac[Nfinal].mean()
            Nc_err = datac[Nfinal].std()
            print('(sm=%.3f, N=%i, bd=%.3f, lw=%.3f, owe=%.3f)' % (float(config['conversion']['measured_sm']), Nc, data.bd.mean(), data.lw.mean(), data.owe.mean()))
            N0     = Calibrate_N0_Desilets(Nc, float(config['conversion']['measured_sm']), bd=data.bd.mean(), lw=data.lw.mean(), owe=data.owe.mean(),
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))       #                Nc/(float(config['conversion']['a0']) / (float(config['conversion']['measured_sm'])/float(config['conversion']['bulk_density']) + float(config['conversion']['a2']) + float(config['conversion']['lattice_water']) + float(config['conversion']['soil_org_carbon'])*0.556) + float(config['conversion']['a1']))
            N0_err = Calibrate_N0_Desilets(Nc+Nc_err, float(config['conversion']['measured_sm']), bd=data.bd.mean(), lw=data.lw.mean(), owe=data.owe.mean(),
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2'])) - N0  #/(float(config['conversion']['a0']) / (float(config['conversion']['measured_sm'])/float(config['conversion']['bulk_density']) + float(config['conversion']['a2']) + float(config['conversion']['lattice_water']) + float(config['conversion']['soil_org_carbon'])*0.556) + float(config['conversion']['a1']))
            
            print(cprintf('i   N0 = %.0f +/- %.0f cph' % (N0, abs(N0_err)), 'cyan')) # todo: make a report using N0 as column
        else:
            N0 = float(N0)
        
        # Conversion
        sm = config['conversion']['new_moisture_column']
        sm_neighbors = sm+'_neighbors'
        
        #while True:
        smv = sm+'_vol'
        smg = sm+'_grv'
        
        print('| Conversion to water content (N0=%i, bd=%.3f, lw=%.3f, owe=%.3f)... ' % (N0, data.bd.mean(), data.lw.mean(), data.owe.mean()))
        data[smg] = N2SM_Desilets(data[Nfinal], N0, bd=1, lw=data.lw, owe=data.owe,
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
        data[smv] = N2SM_Desilets(data[Nfinal], N0, bd=data.bd, lw=data.lw, owe=data.owe,
                    a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
        #debug()
        data[smg+err]        = dtheta_stdx(data[Nfinal], data[Nfinal+err], N0, a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
        data[smg+err+'_low'] = dtheta_low(data[Nfinal], data[Nfinal+err], N0, a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
        data[smg+err+'_upp'] = dtheta_upp(data[Nfinal], data[Nfinal+err], N0, a0=float(config['conversion']['a0']), a1=float(config['conversion']['a1']), a2=float(config['conversion']['a2']))
        #report_SM(data, smg)
        
        data['bd_err'] = 0
        if 'bulk_density_err' in config['conversion']:
            data['bd_err'] = float(config['conversion']['bulk_density_err'])
        data[smv+err]        = errpropag_factor(data, 'bd', smg, err=err)                  #data.bd * data[smg+err]        + data[smg]        * 
        data[smv+err+'_low'] = -errpropag_factor(data, 'bd', smg, err=err, err2=err+'_low') #data.bd * data[smg+err+'_low'] + data[smg+err+'_low'] * float(config['conversion']['bulk_density_err'])
        data[smv+err+'_upp'] = errpropag_factor(data, 'bd', smg, err=err, err2=err+'_upp') #data.bd * data[smg+err+'_upp'] + data[smg+err+'_upp'] * float(config['conversion']['bulk_density_err'])
        #report_SM(data, smv)
        
      
            #(float(config['conversion']['a0'])/(data[N]/int(N0)-float(config['conversion']['a1']))-float(config['conversion']['a2']) \
            #            -float(config['conversion']['lattice_water']) - float(config['conversion']['soil_org_carbon'])*0.556) * float(config['conversion']['bulk_density'])
            
            #if 'Nfinal_neighbors' in data.columns:
            #    #JJ
            #    #data[sm_neighbors] = (float(config['conversion']['a0'])/(data['N_neighbors']/int(N0)-float(config['conversion']['a1']))-float(config['conversion']['a2']) \
            #    #            -float(config['conversion']['lattice_water']) - float(config['conversion']['soil_org_carbon'])*0.556) * float(config['conversion']['bulk_density'])

            #else:
            #    break
                
        # Cut out invalid soil moisture results
        
            
    ######  Clean soil moisture  #######################
    
        validlinesg = len(data[smg].dropna())
        validlinesv = len(data[smv].dropna())
        (a,b) = xsplit(config['clean']['sm_range'], type=float)
        
        if config['clean']['invalid_data'] == 'drop':
            data = data.query("@"+smg+" > %s and @"+smg+" < %s" % (a,b))
            data = data.query("@"+smv+" > %s and @"+smv+" < %s" % (a,b))
        else:
            data.loc[(data[smg] < a) | (data[smg] > b), smg+err] = np.nan
            data.loc[(data[smg] < a) | (data[smg] > b), smg+err+'_low'] = np.nan
            data.loc[(data[smg] < a) | (data[smg] > b), smg+err+'_upp'] = np.nan
            data.loc[(data[smg] < a) | (data[smg] > b), smg] = np.nan
            data.loc[(data[smg]-data[smg+err] < a), smg+err] = data[smg]
            data.loc[(data[smg]+data[smg+err] > 1), smg+err] = 1-data[smg]
            data.loc[(data[smg]+data[smg+err+'_low'] < a), smg+err+'_low'] = -data[smg]
            data.loc[(data[smg]+data[smg+err+'_upp'] > b), smg+err+'_upp'] = 1-data[smg]
            
            data.loc[(data[smv] < a) | (data[smv] > b), smv+err] = np.nan
            data.loc[(data[smv] < a) | (data[smv] > b), smv+err+'_low'] = np.nan
            data.loc[(data[smv] < a) | (data[smv] > b), smv+err+'_upp'] = np.nan
            data.loc[(data[smv] < a) | (data[smv] > b), smv] = np.nan
            data.loc[(data[smv]-data[smv+err] < a), smv+err] = data[smv]
            data.loc[(data[smv]+data[smv+err] > b), smv+err] = 1-data[smv]
            data.loc[(data[smv]+data[smv+err+'_low'] < a), smv+err+'_low'] = -data[smv]
            data.loc[(data[smv]+data[smv+err+'_upp'] > b), smv+err+'_upp'] = 1-data[smv]
            #if sm_neighbors in data.columns:
            #    data.loc[(data[sm_neighbors] < a) | (data[sm_neighbors] > b), sm_neighbors] = np.nan

        data[sm] = data[smg]
        data[sm+'_%'] = data[sm] * 100
    
    #print('dropped %i smg values out of %i' % (validlinesg-len(data[smg].dropna()), validlinesg))
    print('i Dropped %i out of %i values (out of range) from %s.' % (validlinesv-len(data[smv].dropna()), validlinesv, smv))
        
    report_SM(data, smg, units='grv.%%')
    report_SM(data, smv)
    #print('%.3f +/- %.3f -%.3f +%.3f' % (data[smv].mean(), data[smv+err].mean(), data[smv+err+'_low'].mean(), data[smv+err+'_upp'].mean()))

    ######  Penetration Depth #######################

    data['footprint_depth'] = D86(data[smv], data.bd)
    print('i   Footprint depth D86 = %.0f +/- %.0f cm.' % (data.footprint_depth.mean(), data.footprint_depth.std()))
    # Calculate time resolution
    
    tres = data.index.to_series().dropna().diff().median().total_seconds()
    tres_str = ''
    if   tres/86400 >= 1: tres_str = '%.0fd' % (tres/86400)
    elif tres/3600  >= 1: tres_str = '%.0fh' % (tres/3600)
    elif tres/60    >= 1: tres_str = '%.0fm' % (tres/60)
    else                : tres_str = '%.0fs' % (tres)
    
    # Output
    
    if yesno2bool(config['output']['make_CSV']):
        print('| CSV data preparation... ')
        datetime_format = re.sub(r'(\w)', r'%\1', config['output']['CSV_datetime_format'])
        try:
            data.to_csv(output_basename+'.csv',
                date_format = datetime_format,
                sep = config['output']['CSV_column_sep'].strip('\"'),
                decimal = config['output']['CSV_decimal'],
                float_format = '%' + config['output']['CSV_float_format'],
                na_rep = config['output']['CSV_NaN'])
        except:
            print('! ERROR: Cannot write, probably permission denied.')
            
        file_save_msg(output_basename + '.csv', "Data", end="\n")
    
    
    # Figures
    if yesno2bool(config['output']['make_PDF']):

        print('| Plotting... ', end='')
        figure_name = output_basename + '.pdf'
        
        #print(data[N].to_string())
        ## With PdfPages: Create PDF on enter and close PDF on exit
        ## see https://matplotlib.org/gallery/misc/multipage_pdf.html
        with matplotlib.backends.backend_pdf.PdfPages(figure_name) as fig:
            
            Fig.time_format = config['output']['PDF_time_format']
            
            # Neutrons
            with Fig(fig, title='Neutron time series (%s aggregation)' % tres_str, ylabel='Neutrons (in cph)') as ax:
                if not data[N].isnull().all():
                    plt.plot(data[N],           label="N raw counts in cph",     ls='', marker='o', ms=2, markeredgewidth=0, color='red')
                    plt.plot(data[N+clean],     label="N cleaned", ls='', marker='o', ms=3, markeredgewidth=0, color='lightgrey')
                    #plt.plot(data[N+clean+pih], label="N_cc_pih", ls='', marker='o', ms=3,  color='C0', alpha=0.6, markeredgewidth=0) # , drawstyle='steps'
                    #plt.plot(data[N+clean+pih+urb+bio], label="N_cc_pih_ub", ls='', marker='o', ms=3,  color='C1', alpha=0.6, markeredgewidth=0)
                    if Nfinal == N+clean+pih:
                        plt.plot(data[N+clean+pih],                                lw=2, label="N c&corr press,incom,humid", color='black')
                    else:
                        plt.plot(data[N+clean+pih],                                lw=5, label="N c&corr press,incom,humid", color='lightgrey')
                    if urb != '': plt.plot(data[N+clean+pih+poi+urb],              lw=4, label="N c&corr press,incom,humid,urban", color='C6')
                    if bio != '': plt.plot(data[N+clean+pih+poi+urb+bio],          lw=3, label="N c&corr press,incom,humid,urban,bio", color='C2')
                    if road!= '': plt.plot(data[N+clean+pih+poi+urb+bio+road],     lw=2, label="N c&corr press,incom,humid,urban,bio,road", color='C3')
                    if mov != '': plt.plot(data[N+clean+pih+poi+urb+bio+road+mov], lw=1, label="N c&corr press,incom,humid,urban,bio,road,smoothed", color='black')
                    #if Nfinal != N+clean+pih:
                    #    plt.plot(data[Nfinal], label="N cleaned corrected_pih smoothed_%s" % config['clean']['smooth'], color='C0')
                    ax.fill_between(data.index, data[Nfinal]-data[Nfinal+'_err'], data[Nfinal]+data[Nfinal+'_err'], color='black', alpha=0.2, lw=0)
                    #plot_errorbar(data.index, data[Nfinal], xerr=None, yerr=data[Nfinal+err], fmt='o', ecolor='lightgrey', elinewidth=1, capsize=0, mfc='black', ms=3, mew=0)
                    
                else:
                    plt.plot(data.index[0], 0)

                if 'N_neighbors' in data.columns:
                    #JJ
                    if not data['N_neighbors'].isnull().all():
                        plt.plot(data['N_neighbors'], label='N_neighbors: spatially smoothed', drawstyle='steps')
                        
                plt.legend()

                

        #            if N+'_std' in data.columns:
        #                ax.fill_between(data.index, data[N]-data[N+'_std'], data[N]+data[N+'_std'], alpha=0.25, lw=0, step='pre')

#            if Nth:
#                with Fig(fig, title='Thermal neutron time series, corrected', ylabel='Thermal neutrons (in cph)') as ax:
#                    if not data[Nth].isnull().all():
#                        plt.plot(data[Nth], label="N_thermal", drawstyle='steps')

            
            # Corrections
            with Fig(fig, title='Corrections', ylabel='-') as ax:
                if not data[N].isnull().all():
                    plt.plot(data['correct_p'], label='air pressure', color='grey')
                    plt.plot(data['correct_i'], label='incoming neutron flux', color='C1')
                    plt.plot(data['correct_h'], label='air humidity', color='C0')
                    plt.plot(data['correct_urban'], label='urban area', lw=5, color='C6')
                    plt.plot(data['correct_bio'], label='biomass', color='C2')
                    plt.plot(data['correct_road'], label='road effect', color='black', lw=1)
                    plt.legend()
                else:
                    plt.plot(data.index[0], 0)

                if config['conversion']['road_method'] != '' and config['conversion']['road_method'] != 'None'and lat in data.columns:
                    with Fig(fig, title='Road parameters', ylabel='-') as ax: 
                        plt.plot(data['road_width'], label='road_width', marker='o', ls='', ms=2)
                        plt.plot(data['road_moisture'], label='road_moisture', marker='o', ls='', ms=2)
                        plt.legend()
                
            with Fig(fig, title='Soil properties', ylabel='-') as ax: 
                plt.plot(data['bd'], label='bulk density', marker='o', ls='', ms=2)
                plt.plot(data['lw'], label='lattice water', marker='o', ls='', ms=2)
                plt.plot(data['owe'], label='Organic water equivalent', marker='o', ls='', ms=2)
                plt.legend()

            # Air properties
            with Fig(fig, layout=(2,2)) as axes:
                if not data[p].isnull().all(): 
                    with Fig(ylabel='Air pressure (in mbar)'):
                        plt.plot(data[p], drawstyle='steps')
                else:
                    pass#if len(list(axes))>=1: axes[1].axis('off')
                
                if not data[T].isnull().all(): 
                    with Fig(ylabel='Air temperature (in $^\circ$C)'):
                        plt.plot(data[T], drawstyle='steps')
                else:
                    pass#if len(list(axes))>=2: axes[2].axis('off')
                
                if not data[rh].isnull().all(): 
                    with Fig(ylabel='Relative humidity (in %)'):
                        plt.plot(data[rh], drawstyle='steps')
                else:
                    pass#if len(list(axes))>=3: axes[3].axis('off')
                
                if not data[ah].isnull().all(): 
                    with Fig(ylabel='Absolute humidity (in g/m$^3$)'):
                        plt.plot(data[ah], drawstyle='steps')
                else:
                    pass#if len(list(axes))>=4: axes[4].axis('off')

            # Moisture
            if config['conversion']['N2sm_method'] != 'None':     
                if not data[sm].isnull().all():
                        
#                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content') as ax:
#                        plt.plot(data[Nfinal])
#                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content') as ax:
#                        plt.plot(data[Nfinal+err])
#                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content') as ax:
#                        plt.plot(data[smg])
#                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content') as ax:
#                        plt.plot(data[smg+err])
    
                    
                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content', ylim=(0,1)) as ax:        
                        plt.plot(data[smg], color='black', ls='', marker='o', ms=4, markeredgewidth=0)
                        #ax.fill_between(data.index, data[smg]+data[smg+err+'_low'], data[smv]+data[smg+err+'_upp'], color='C0', alpha=0.4, lw=0, step='pre')
                        plot_errorbar(data.index, data[smg], xerr=None, yerr=[-data[smg+err+'_low'], data[smg+err+'_upp']], fmt='o', ecolor='lightgrey', elinewidth=1, capsize=0, mfc='black', ms=3, mew=0)
                        plt.yticks(np.arange(0,1.1, 0.1)) 
                        
                    with Fig(fig, title='Water content time series, corrected', ylabel='Volumetric water content', ylim=(0,1)) as ax:        
                        plt.plot(data[smv], color='black', ls='', marker='o', ms=4, markeredgewidth=0)
                        #ax.fill_between(data.index, data[smv]+data[smv+err+'_low'], data[smv]+data[smv+err+'_upp'], color='C0', alpha=0.4, lw=0, step='pre')
                        plot_errorbar(data.index, data[smv], xerr=None, yerr=[data[smv+err+'_low'], data[smv+err+'_upp']], fmt='o', ecolor='lightgrey', elinewidth=1, capsize=0, mfc='None', ms=3, mew=1)
                        plt.yticks(np.arange(0,1.1, 0.1)) 

                        
#                        data[smv+'_int'] = data[smv].interpolate()
#                        data[smv+err+'_low_int'] = data[smv+err+'_low'].interpolate()
#                        data[smv+err+'_upp_int'] = data[smv+err+'_upp'].interpolate()
#                        data = moving_avg(data, smv+'_int', window=15, err=False, std=False)
#                        data = moving_avg(data, smv+err+'_low_int', window=15, err=False, std=False)
#                        data = moving_avg(data, smv+err+'_upp_int', window=15, err=False, std=False)
                        
#                        plt.plot(data[smv+'_int'+mov], color='black', alpha=0.3, drawstyle='steps')
                        #plt.plot(data[smv+'_int'+mov]+data[smv+err+'_low_int'+mov], color='red')
                        #plt.plot(data[smv+'_int'+mov]+data[smv+err+'_upp_int'+mov], color='red')
#                        ax.fill_between(data.index, data[smv+'_int'+mov]+data[smv+err+'_low_int'+mov],
#                                                    data[smv+'_int'+mov]+data[smv+err+'_upp_int'+mov],
#                                                    color='black', alpha=0.1, lw=0, step='pre')
                        
                        #plt.plot(data[smv] +data[smv+err+'_low'], color='C1', ls='', marker='o', ms=2, markeredgewidth=0)
                        #plt.plot(data[smv] +data[smv+err+'_upp'], color='C1', ls='', marker='o', ms=2, markeredgewidth=0)
                        
#                        plt.errorbar(data.index, data[smv], yerr=[data[smv] +data[smv+err+'_low'], data[smv] +data[smv+err+'_upp']],
#                                     fmt='s', ecolor='black', elinewidth=1, capsize=0, mfc='black', mec='black', ms=4, mew=2)
                        
#                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content', ylim=(0,1)):        
#                        plt.plot(data[smg], color='black', drawstyle='steps')
#                        plt.plot(data[smg] +data[smg+err+'_low'], color='C1', drawstyle='steps')
#                        plt.plot(data[smg] +data[smg+err+'_upp'], color='C1', drawstyle='steps')
                        
                        
#                        ax.fill_between(data.index, data[smv]+data[smv+err+'_low'], data[smv]+data[smv+err+'_upp'], color='C0', alpha=0.4, lw=0, step='pre')
                        #plt.plot(data[smv] -data[smv+err], color='C2')
                        #plt.plot(data[smv] +data[smv+err], color='C2')
#                        if calibration:
#                            plt.plot([datac.index.min(), datac.index.max()], [float(config['conversion']['measured_sm'])*100, float(config['conversion']['measured_sm'])*100], color='red', lw=10, alpha=0.5)
#                            plt.text(datac.index.min(), float(config['conversion']['measured_sm'])*105, 'calibration', rotation=90, va='bottom', color='red', alpha=0.5)
                    
#                    datax = data.copy()
#                    datax[smv] = datax[smv].interpolate()
                    #datax = moving_avg(datax, smv, window=15, err=False, std=False)
#                    datax[smv+err+'_low'] = datax[smv+err+'_low'].interpolate()
                    #datax = moving_avg(datax, smv+err+'_low', window=15, err=False, std=False)
#                    datax[smv+err+'_upp'] = datax[smv+err+'_upp'].interpolate()
                    #datax = moving_avg(datax, smv+err+'_upp', window=15, err=False, std=False)
#                    datay = minly(datax,5)
#                    datay = moving_avg(datay, smv, window=6, err=False, std=False)
#                    datay = moving_avg(datay, smv+err+'_low', window=6, err=False, std=False)
#                    datay = moving_avg(datay, smv+err+'_upp', window=6, err=False, std=False)
#                    datay = moving_avg(datay, smg, window=6, err=False, std=False)
#                    datay = moving_avg(datay, smg+err+'_low', window=6, err=False, std=False)
#                    datay = moving_avg(datay, smg+err+'_upp', window=6, err=False, std=False)

#                    with Fig(fig, title='Water content time series, corrected', ylabel='Volumetric water content', ylim=(0,1)) as ax:        
#                        plt.plot(datay[smv+mov], color='black', drawstyle='steps')
                        #plt.plot(datay[smv+mov] +datay[smv+err+'_low'+mov], color='C1', drawstyle='steps')
                        #plt.plot(datay[smv+mov] +datay[smv+err+'_upp'+mov], color='C1', drawstyle='steps')
#                        ax.fill_between(datay.index, datay[smv+mov]+datay[smv+err+'_low'+mov], datay[smv+mov]+datay[smv+err+'_upp'+mov], color='C0', alpha=0.4, lw=0, step='pre')
                    
#                    with Fig(fig, title='Water content time series, corrected', ylabel='Gravimetric water content', ylim=(0,1)) as ax:        
#                        plt.plot(datay[smg+mov], color='black', drawstyle='steps')
#                        ax.fill_between(datay.index, datay[smg+mov]+datay[smg+err+'_low'+mov], datay[smg+mov]+datay[smg+err+'_upp'+mov], color='C0', alpha=0.4, lw=0, step='pre')
                             
                    # hist
                    print('Histogram... ', end='')
                    with Fig(fig, title='PDF '+sm, ylabel='probability density', time_series=False, size=(10,5)) as ax:
                        ax.hist(data[sm].dropna().values, bins=100, density=True, histtype='stepfilled')
                        ax.set_xlim(0,1)
                        ax.set_xticks(np.arange(0,1,0.1))
                        ax.set_xlabel('vol. soil moisture (in m$^3$/m$^3$)')
                        #ax.set_ylabel('probability density')
                        #ax.set_title('Soil moisture PDF') ;
                        ax.grid(color='black', alpha=0.1)
                    


            # More plots
            for var in xsplit(config['output']['PDF_plots']):
                if var in data.columns:
                    try:
                        with Fig(fig, title=var, ylabel=var):        
                            if not data[var].isnull().all(): 
                                if not data[var].isna().all():
                                    plt.plot(data[var], ls='', marker='o', ms=5, markeredgewidth=0) #drawstyle='steps')
                    except ValueError:
                        print('Formating problem, subplot for variable ',var,' skipped while creating pdf')
                            
            # Moisture interpolation
            var = sm

                                          
                  
                                                                                                                
                                                                                        
                                
                                                 
                                                                    
                                                     
                                                    
                                                 
            
            if lat != '' and lat in data.columns:
                datax = data[[lon,lat,var]].dropna()
                if len(datax)==0:
                    raise ValueError('No records with valid sm left after processing, stop printing')
                from pyproj import Transformer
                xs, ys = Transformer.from_crs("epsg:4326", "epsg:31468").transform(datax[lat].values, datax[lon].values)
                datax['y'] = xs
                datax['x'] = ys
            
                from scipy.interpolate import griddata
                # data coordinates and values
                x = datax.x.values - datax.x.min()
                y = datax.y.values - datax.y.min()
                z = datax[var].values

                # config
                print('XY plot...', end='')
                resolution = 20 
                smrange = (0,0.5)
                contour_levels = np.arange(0.05,0.525,0.025) #20

                xrange = x.max()-x.min()
                if xrange==0:
                    xrange=1
                
                yrange = y.max()-y.min()
                if yrange==0:
                    yrange=1
                
                xpad = 0.05*xrange
                ypad = 0.05*yrange

                mysize = (10,10/xrange*yrange)
                if mysize[1] > 20:
                    mysize = (10*xrange/yrange,10)
                with Fig(fig, title='Map '+var, xlabel='Easting (in m)', ylabel='Northing (in m)', size=mysize, time_series=False) as ax:
                    q = ax.scatter( x, y, c=z, cmap='Spectral', vmin=smrange[0], vmax=smrange[1])
                    ax.set_aspect(1)
                    ax.set_xlim(x.min()-xpad,x.max()+xpad)
                    ax.set_ylim(y.min()-ypad,y.max()+ypad)
                    #ax.set_xlabel('Easting [m]')
                    #ax.set_ylabel('Northing [m]')
                    #ax.set_title('Soil Moisture Map') ;
                    cb = plt.colorbar(q, extend="both", ax=ax, shrink=0.6, pad=0.03, aspect=30)
                    cb.set_label("vol. soil moisture (in m$^3$/m$^3$)", rotation=270, labelpad=20)
                    plt.grid(color='black', alpha=0.1)


            # target grid to interpolate to
#            print('Interpolate...')
#            xi = np.arange(x.min()-xpad,x.max()+xpad, resolution)
#            yi = np.arange(y.min()-ypad,y.max()+ypad, resolution)
#            xi,yi = np.meshgrid(xi,yi)
            # interpolate
#            zi = griddata((x,y),z,(xi,yi), method='linear') #, fill_value=0.3
            # plot
#            with Fig(fig, title='Lin. Interpolation on %.0f m grid, %s' % (resolution, var), xlabel='Easting (in m)', ylabel='Northing (in m)', size=(10,10/(xrange/yrange)**2), time_series=False) as ax:
#                q = ax.contourf(xi, yi, zi, cmap='Spectral', vmin=smrange[0], vmax=smrange[1], levels=contour_levels, extend='both')
#                ax.plot(x, y,'k.', ms=1)
                #ax.set_xlabel('Easting [m]', fontsize=10)
                #ax.set_ylabel('Northing [m]', fontsize=10)
                #ax.set_title('Soil moisture linear interpolation')
#                cb = plt.colorbar(q, extend="both", ax=ax, shrink=0.6, pad=0.03, aspect=30) #, ticks=np.arange(0,0.5,0.05)) #ticks=np.arange(np.ceil(np.nanmin(zi)*20)/20,np.nanmax(zi),0.05)
#                cb.set_label("vol. soil moisture (in m$^3$/m$^3$)", rotation=270, labelpad=20)
#                plt.grid(color='black', alpha=0.1)

                            
        
        # Done
        print('OK')
        file_save_msg(figure_name, "Figures", end="\n")
        
    # Make KML if lat/lon is provided
    if lat in data.columns and lon in data.columns:
    
        if yesno2bool(config['output']['make_KML_neutrons']):
            if not data[Nfinal].isnull().all():
                print('| Creating_KML for %s... ' % Nfinal)
                collim = xsplit(config['output']['KML_neutrons_range'])
                if len(collim)<=1: collim = None
                save_KML(data, Nfinal, lat, lon, file=output_basename+'-'+config['correction']['new_neutron_column'], format_str='', cmap='Spectral', collim=collim)#(3000,9000)) # long track: 5000,11000
                file_save_msg(output_basename+'-'+config['correction']['new_neutron_column']+'.kml', 'KML', end="\n")
            
#            if Nth:
#                print("\n"+'Creating_KML... ', end='')
#                save_KML(data, Nth, lat, lon, file=output_basename+'-'+config['correction']['new_neutron_column']+'_thermal', format_str='', cmap='Spectral') #, collim=(3000,9000)) # long track: 5000,11000
#                file_save_msg(output_basename+'-'+config['correction']['new_neutron_column']+'_thermal.kml', '', end='')
        
        if yesno2bool(config['output']['make_KML_sm']):
            if not data[sm].isnull().all():
                print('| Creating_KML for %s... ' % sm)
                collim = xsplit(config['output']['KML_sm_range'])
                if len(collim)<=1: collim = None
                save_KML(data, sm+'_%', lat, lon, file=output_basename+'-'+sm, format_str='%.0f%%', collim=collim)#(5,80))
                file_save_msg(output_basename+'-'+sm+'.kml', 'KML', end="\n")
        
        if yesno2bool(config['output']['make_KML_other']):
            other = config['output']['KML_other_column']
            if other in data.columns:
                if not data[other].isnull().all():
                    print('| Creating_KML for %s... ' % other)
                    collim = xsplit(config['output']['KML_other_range'])
                    if len(collim)<=1: collim = None
                    if yesno2bool(config['output']['KML_other_reverse']):
                        cmap = 'Spectral_r'
                    else:
                        cmap = 'Spectral'
                    format_str = '%'+config['output']['KML_other_format'] if config['output']['KML_other_format'] else ''
                    save_KML(data, other, lat, lon, file=output_basename+'-'+other, format_str=format_str, cmap=cmap, collim=collim)#(5,80))
                    file_save_msg(output_basename+'-'+other+'.kml', 'KML', end="\n")
            
         # Contourf Interpolation    
#        P = P[P.LatDec>47.829]
#        P = P[P.N_smooth > 0]
#        ngridx = int(10000*(P.LongDec.max()-P.LongDec.min()))
#        ngridy = int(10000*(P.LatDec.max()-P.LatDec.min()))
#        print(ngridx, ngridy)
#        x = [int(10000*x) for x in (P.LongDec-P.LongDec.min())]
#        y = [int(10000*x) for x in (P.LatDec-P.LatDec.min())]
#        z = P.moisture.interpolate().values*100
#        xi = np.linspace(0, ngridx, ngridx)
#        yi = np.linspace(0, ngridy, ngridy)
#        triang = tri.Triangulation(x, y)
#        interpolator = tri.LinearTriInterpolator(triang, z)
#        Xi, Yi = np.meshgrid(xi, yi)
#        zi = interpolator(Xi, Yi) 
#        fig, ax = plt.subplots(figsize=(10,5))
#        plt.contourf(zi, cmap='Spectral', levels=10)
#        plt.scatter(x,y, color='black', s=1)
    plt.close('all')        
    print("= Done.")
    return(data)

# Direct call (instead of import)
if __name__ == "__main__":
    param2 = '' 
    if len(sys.argv) > 1:
        main(sys.argv.pop(1))
        if len(sys.argv) > 1:
            param2 = sys.argv.pop(1)
        if param2 != '--nonstop':
            print("Press any key to close.", end='')
            input()
    else:
        main()
    
    if param2 != '--nonstop':
        print("Press any key to close.", end='')
        input()
    
